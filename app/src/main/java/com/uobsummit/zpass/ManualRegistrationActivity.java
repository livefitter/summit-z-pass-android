package com.uobsummit.zpass;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.zxing.common.BitMatrix;
import com.uobsummit.zpass.constants.API;
import com.uobsummit.zpass.models.WalkInUser;
import com.uobsummit.zpass.models.Workshop;
import com.uobsummit.zpass.preferences.SecurePreferencesHelper;
import com.uobsummit.zpass.requests.VolleyInstance;
import com.uobsummit.zpass.utils.ZpassUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class ManualRegistrationActivity extends AppCompatActivity
        implements TextView.OnEditorActionListener,
        View.OnClickListener{

    private static final String TAG = "ManualRegistration";

    static final int REQUEST_DIGITAL_SIGNATURE = 1337;

    public static final String KEY_WORKSHOP = "ManualRegistration_workshop";
    private static final String KEY_NAME = "entered_name";
    private static final String KEY_EMAIL = "entered_email";
    private static final String KEY_EMPLOYEE_ID = "entered_employee_id";
    private static final String KEY_CONTACT_NUMBER = "entered_contact_number";
    private static final String KEY_WALK_IN_USER = "walk_in_user";

    // UI elements
    TextInputEditText mNameEdit, mEmailEdit, mEmployeeIDEdit, mContactNumberEdit;
    CheckBox mCheckbox;
    LinearLayout mEmployeeDetailsLayout;
    TextView mStatusText, mNameText, mCreditsText, mCreditCostText, mCommentsText;
    Button mConfirmButton, mAcceptButton, mDeclineButton;
    FrameLayout mProgress;
    AppCompatDialog mDialog;

    Workshop mWorkshop;
    WalkInUser mWalkInUser;

    // API request
    ValidateManualRegistrationTasks mValidationTask;
    AcceptUserTask mAcceptUserTask;

    Resources res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_registration);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        res = getResources();

        initUIReferences();

        Bundle extras = getIntent().getExtras();

        // Instantiate the workshop variable
        Workshop workshop = null;
        if(savedInstanceState != null){
            Log.d("~~", "Restoring from saved instance state");
            workshop = savedInstanceState.getParcelable(KEY_WORKSHOP);
            mNameEdit.setText(savedInstanceState.getString(KEY_NAME, ""));
            mEmailEdit.setText(savedInstanceState.getString(KEY_EMAIL, ""));
            mEmployeeIDEdit.setText(savedInstanceState.getString(KEY_EMPLOYEE_ID, ""));
            mContactNumberEdit.setText(savedInstanceState.getString(KEY_CONTACT_NUMBER, ""));
            mWalkInUser = savedInstanceState.getParcelable(KEY_WALK_IN_USER);
        }else if(extras != null){
            Log.d("~~", "Initializing workshop values");
            workshop = extras.getParcelable(KEY_WORKSHOP);
        }

        if(workshop != null){
            mWorkshop = workshop;
        }
    }

    /**
     * Retrieve all references for the UI
     * and call their relevant listeners
     */
    private void initUIReferences(){

        // EditText views
        mNameEdit = (TextInputEditText) findViewById(R.id.manual_name);
        mEmailEdit = (TextInputEditText) findViewById(R.id.manual_email);
        mEmployeeIDEdit = (TextInputEditText) findViewById(R.id.manual_employee_id);
        // Get a reference to the Contact number editText field
        // and listen to its editor actions to close the soft keyboard when they press "DONE"
        mContactNumberEdit = (TextInputEditText) findViewById(R.id.manual_contact_number);
        mContactNumberEdit.setOnEditorActionListener(this);

        // Agreement checkbox
        mCheckbox = (CheckBox) findViewById(R.id.manual_checkbox_agree);
        mCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                Log.d("~~", "Toggled checkbox value: " + isChecked);
                if(mConfirmButton != null) {
                    mConfirmButton.setEnabled(isChecked);
                }
            }
        });

        // Employee details part
        mEmployeeDetailsLayout = (LinearLayout) findViewById(R.id.manual_layout_employee_details);
        mStatusText = (TextView) findViewById(R.id.manual_text_status);
        mNameText = (TextView) findViewById(R.id.manual_text_name);
        mCreditsText = (TextView) findViewById(R.id.manual_text_credit_remaining);
        mCreditCostText = (TextView) findViewById(R.id.manual_text_credit_cost);
        mCommentsText = (TextView) findViewById(R.id.manual_text_comments);

        // Buttons
        mConfirmButton = (Button) findViewById(R.id.manual_button_confirm);
        mAcceptButton = (Button) findViewById(R.id.manual_button_accept);
        mDeclineButton = (Button) findViewById(R.id.manual_button_decline);

        // Button click listeners
        mConfirmButton.setOnClickListener(this);
        mAcceptButton.setOnClickListener(this);
        mDeclineButton.setOnClickListener(this);

        mProgress = (FrameLayout) findViewById(R.id.manual_progress);

        //Hide the employee details
        showEmployeeDetails(false);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Store the mWorkshop value
        outState.putParcelable(KEY_WORKSHOP, mWorkshop);
        outState.putString(KEY_NAME, mNameEdit.getText().toString());
        outState.putString(KEY_EMAIL, mEmailEdit.getText().toString());
        outState.putString(KEY_EMPLOYEE_ID, mEmployeeIDEdit.getText().toString());
        outState.putString(KEY_CONTACT_NUMBER, mContactNumberEdit.getText().toString());
        outState.putParcelable(KEY_WALK_IN_USER, mWalkInUser);

        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
        boolean handled = false;
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            ZpassUtil.hideSoftKeyboard(this);
            textView.clearFocus();
            mCheckbox.requestFocus();
            handled = true;
        }

        return handled;
    }

    @Override
    public void onClick(View view) {
        // Hide the keyboard no matter what
        ZpassUtil.hideSoftKeyboard(this);
        if(view == mDeclineButton){
            setResult(RESULT_CANCELED);
            finish();
        }else if(view == mAcceptButton) {
            if (mWorkshop.isCoreWorkshop() && mWalkInUser != null){
                // Get digital signature
                goToDigitalSignature();
            }else{
                acceptWalkInUser();
            }

        }else if(view == mConfirmButton){
            if(validateFormFields()) {
                validateManualRegistration();
            }
        }
    }

    private boolean validateFormFields() {

        // Check for required fields
        if(mNameEdit.getText().length() == 0){
            mNameEdit.setError(String.format(getString(R.string.error_field_required), getString(R.string.label_name)));
            mNameEdit.requestFocus();
            return false;
        }else if(mEmailEdit.getText().length() == 0){
            mEmailEdit.setError(String.format(getString(R.string.error_field_required), getString(R.string.label_email)));
            mEmailEdit.requestFocus();
            return false;
        }else if(mEmployeeIDEdit.getText().length() == 0){
            mEmployeeIDEdit.setError(String.format(getString(R.string.error_field_required), getString(R.string.label_employee_id)));
            mEmployeeIDEdit.requestFocus();
            return false;
        }else if(mContactNumberEdit.getText().length() == 0){
            mContactNumberEdit.setError(String.format(getString(R.string.error_field_required), getString(R.string.label_contact_number)));
            mContactNumberEdit.requestFocus();
            return false;
        }

        // Check for incorrect email format
        if(mEmailEdit.getText().length() > 0){
            String email = mEmailEdit.getText().toString();

            if(!email.contains("@")){
                mEmailEdit.setError(getString(R.string.error_invalid_email));
                mEmailEdit.requestFocus();
                return false;
            };

            // Change entire text to be non capitalized
            String emailContent = mEmailEdit.getText().toString();
            mEmailEdit.setText(emailContent.toLowerCase());
        }

        return true;
    }

    private void showEmployeeDetails(boolean showLayout){
        if(mEmployeeDetailsLayout != null) {
            int visibility = showLayout ? View.VISIBLE : View.GONE;

            mEmployeeDetailsLayout.setVisibility(visibility);
        }
    }

    private void showProgress(boolean showLayout){
        if(mProgress != null) {
            int visibility = showLayout ? View.VISIBLE : View.GONE;

            mProgress.setVisibility(visibility);
        }
    }

    /**
     * Calls the API request to validate the manual registration
     */
    private void validateManualRegistration() {

        // show the progress bar and hide everything else
        showProgress(true);

        String email = mEmailEdit.getText().toString();
        String employeeID = mEmployeeIDEdit.getText().toString();
        int eventID = mWorkshop.getId();
        final String authEmail = SecurePreferencesHelper.retrieveEmail(this);
        final String authToken = SecurePreferencesHelper.retrieveAuthToken(this);

        if(mValidationTask != null){
            mValidationTask.cancel(true);
        }

        mValidationTask = new ValidateManualRegistrationTasks(this, email, employeeID, eventID, authEmail, authToken);
        mValidationTask.execute();

        /*// Prepare the parameters. We pass in an empty value for "category" since we want to get all Summit workshops
        String params = String.format(API.ValidateManualRegistration.PARAM_EMAIL + "=%1$s&"
                        + API.ValidateManualRegistration.PARAM_EMPLOYEE_ID + "=%2$s&"
                        + API.ValidateManualRegistration.PARAM_EVENT_BATCH_ID + "=%3$s&"
                        + API.ValidateManualRegistration.PARAM_USER_EMAIL + "=%4$s&"
                        + API.ValidateManualRegistration.PARAM_USER_TOKEN + "=%5$s",
                email,
                employeeID,
                eventID,
                authEmail,
                authToken);

        Log.d("~~", "--- params: " + params);

        String url = API.url() + API.ValidateManualRegistration.method() + params;

        Log.d("~~", "--- url: " + url);

        if(mRequest != null){
            mRequest.cancel();
        }

        mRequest = new StringRequest(API.ValidateManualRegistration.httpMethod(), url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i(TAG, "Received response. Parsing JSON");

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject != null ){


                        Log.d("~~", "--- response: " + jsonObject);

                        showProgress(false);
                        boolean kumpleto = (jsonObject.has("whitelist") && jsonObject.has("credit") && jsonObject.has("name") &&
                                jsonObject.has("employee_id"));

                        Log.d("~~", "--- kumpeto? " + kumpleto);

                        // Check if the json response has complete fields
                        if(jsonObject.has("whitelist") && jsonObject.has("credit") && jsonObject.has("name") &&
                                jsonObject.has("employee_id")){
                            //disable the form fields first
                            enableFormFields(false);

                            String userName = jsonObject.getString("name");
                            String employeeID = jsonObject.getString("employee_id");
                            int creditRemaining = jsonObject.getInt("credit");
                            String email = mEmailEdit.getText().toString();
                            String contactNumber = mContactNumberEdit.getText().toString();
                            boolean isInWhitelist = jsonObject.getBoolean("whitelist");
                            String comments = "";

                            if(jsonObject.has("comments")) {
                                comments = retrieveJSONComments(jsonObject.getJSONArray("comments"));
                            }

                            String statusMessage = "";

                            showEmployeeDetails(true);

                            mAcceptButton.setEnabled(true);
                            mDeclineButton.setEnabled(true);

                            // Determine the status color and set the status
                            *//*
                                Cases:
                                1. Whitelist: FALSE, enough credits - User is eligible: The user is eligible, registered with Summit and has enough credits
                                2. Whitelist:TRUE- User has not created an account: The user is eligible, not registered with Summit but has enough credits
                                3. Whitelist:FALSE, not enough credits- User has insufficient credits: The user is eligible, registered with Summit but does not have enough credits
                             *//*
                            boolean hasEnoughCredits = creditRemaining - mWorkshop.getCreditValue() >= 0;
                            mStatusText.setTextColor(res.getColor(R.color.textColorCorrect));
                            // Determine the status message
                            String status = "";

                            if(isInWhitelist){
                                //Case 2
                                status = res.getString(R.string.label_user_no_account);
                                statusMessage = WalkInUser.STATUS_NO_ACCOUNT;
                            }else if(hasEnoughCredits){
                                //Case 1
                                status = res.getString(R.string.label_user_eligible);
                                statusMessage = WalkInUser.STATUS_ELIGIBLE;
                            }else{
                                // Case 3
                                status = res.getString(R.string.label_user_insufficient_credits);
                                statusMessage = WalkInUser.STATUS_INSUFFICIENT_CREDITS;
                            }
                            mStatusText.setText(status);

                            mNameText.setText(userName);
                            mCreditsText.setText(String.valueOf(creditRemaining));
                            mCreditCostText.setText(String.valueOf(mWorkshop.getCreditValue()));
                            mCommentsText.setText(comments);

                            // Check if any of the WalkInUser fields are empty
                            // and fill them up based on the input fields
                            if(userName.isEmpty()){
                                userName = mNameEdit.getText().toString();
                            }
                            if(employeeID.isEmpty()){
                                employeeID = mEmployeeIDEdit.getText().toString();
                            }

                            // Create the walkInUser object too
                            mWalkInUser = new WalkInUser(userName,
                                    employeeID,
                                    creditRemaining,
                                    email,
                                    contactNumber,
                                    isInWhitelist,
                                    comments,
                                    statusMessage);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    showProgress(false);
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error);
                showProgress(false);

                String body = "";

                //get response body and parse with appropriate encoding
                if(error.networkResponse.data!=null) {
                    try {
                        body = new String(error.networkResponse.data,"UTF-8");

                        JSONObject errorMessage = new JSONObject(body);

                        if(errorMessage.has("errors") && !errorMessage.getString("errors").isEmpty()){
                            //showDialogAlert(res.getString(R.string.label_error), errorMessage.getString("errors"));

                            //enable the form fields first and show employee details
                            enableFormFields(true);
                            showEmployeeDetails(true);

                            mAcceptButton.setEnabled(false);
                            mDeclineButton.setEnabled(false);

                            // Get the status message and set the color to the incorrect color
                            String statusMessage = errorMessage.getString("errors");
                            mStatusText.setTextColor(res.getColor(R.color.textColorIncorrect));
                            mStatusText.setText(statusMessage);
                        }else if(errorMessage.has("base") && !errorMessage.getString("base").isEmpty()){
                            //showDialogAlert(res.getString(R.string.label_error), errorMessage.getString("base"));

                            //enable the form fields first and show employee details
                            enableFormFields(true);
                            showEmployeeDetails(true);

                            mAcceptButton.setEnabled(false);
                            mDeclineButton.setEnabled(false);

                            // Get the status message and set the color to the incorrect color
                            String statusMessage = errorMessage.getString("base");
                            mStatusText.setTextColor(res.getColor(R.color.textColorIncorrect));
                            mStatusText.setText(statusMessage);
                        }else{
                            enableFormFields(true);
                            showEmployeeDetails(false);

                            // Generic error
                            showDialogAlert(res.getString(R.string.label_error), res.getString(R.string.error_generic_try_again));
                        }
                    } catch (JSONException | UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        VolleyInstance.getInstance(this).addToRequestQueue(mRequest);*/

    }

    /**
     * Utility method to extract the comments from a JSONArray and convert it into a String list
     * @param commentsJSON JSON Array of comments
     */
    private String retrieveJSONComments(JSONArray commentsJSON) throws JSONException {
        String commentsList = "";

        for(int i = 0; i < commentsJSON.length(); i++){
            commentsList = commentsList.concat(commentsJSON.getString(i));

            if(i < commentsJSON.length() - 1){
                commentsList = commentsList.concat("\n");
            }
        }

        return commentsList;
    }

    /**
     * Starts the Digital Signature activity for a result
     */
    private void goToDigitalSignature() {
        Log.d(TAG, "Core Workshop detected. Requiring Digital signature");

        Intent intent = new Intent(this, DigitalSignatureActivity.class);
        intent.putExtra(DigitalSignatureActivity.KEY_REGISTRATION_MODE, DigitalSignatureActivity.MODE_MANUAL_REGISTRATION);
        intent.putExtra(DigitalSignatureActivity.KEY_WORKSHOP, mWorkshop);
        intent.putExtra(DigitalSignatureActivity.KEY_WALK_IN_USER, mWalkInUser);
        startActivityForResult(intent, REQUEST_DIGITAL_SIGNATURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("~~", "Request Code " + resultCode + "the same?: " + (requestCode == REQUEST_DIGITAL_SIGNATURE) + ", resultCode OK?: " + (resultCode == RESULT_OK) );
        if(requestCode == REQUEST_DIGITAL_SIGNATURE){
            if(resultCode == RESULT_OK){
                // We are finish here, return to the ScanAttendanceActivity
                setResult(RESULT_OK);
                finish();
            }else{
                showDialogAlert(res.getString(R.string.label_error), res.getString(R.string.error_manual_signature_not_captured));
            }
        }
    }

    /**
     * Call the API request to accept the walk in participant
     */
    private void acceptWalkInUser(){

        mProgress.setVisibility(View.VISIBLE);

        String email = SecurePreferencesHelper.retrieveEmail(this);
        String authToken = SecurePreferencesHelper.retrieveAuthToken(this);

        if(mAcceptUserTask != null){
            mAcceptUserTask.cancel(true);
        }

        mAcceptUserTask = new AcceptUserTask(this, email, authToken);
        mAcceptUserTask.execute();

        /*// Prepare the parameters.
        String params = String.format(API.ManualRegistration.PARAM_USER_EMAIL + "=%1$s&"
                        + API.ManualRegistration.PARAM_USER_TOKEN + "=%2$s",
                email,
                authToken);

        Log.d("~~", "--- params: " + params);

        String url = API.url() + API.ManualRegistration.method() + params;

        JSONObject jsonParams = null;
        try {
            jsonParams = generateJSONParams();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Log.d("~~", "--- jsonParams: " + jsonParams);

        if(mCheckInRequest != null){
            mCheckInRequest.cancel();
        }

        mCheckInRequest = new JsonObjectRequest(API.ManualRegistration.httpMethod(),
                url, jsonParams, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                Log.d("~~", "--- response: " + response);

                setResult(RESULT_OK);
                finish();

                mProgress.setVisibility(View.GONE);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                showDialogAlert(res.getString(R.string.label_error), res.getString(R.string.error_generic_try_again));
            }
        }) {

            *//**
             * Passing some request headers
             * *//*
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };

        VolleyInstance.getInstance(this).addToRequestQueue(mCheckInRequest);*/
    }

    private JSONObject generateJSONParams() throws JSONException {
        JSONObject params = new JSONObject();

        // Create the walk in user
        JSONObject walkInUserParams = new JSONObject();
        walkInUserParams.put(API.ManualRegistration.JSON_USER_EMAIL, mWalkInUser.getEmail());
        walkInUserParams.put(API.ManualRegistration.JSON_EVENT_BATCH_CREDIT, mWorkshop.getCreditValue());
        walkInUserParams.put(API.ManualRegistration.JSON_USER_NAME, mWalkInUser.getName());
        walkInUserParams.put(API.ManualRegistration.JSON_EMPLOYEE_ID, mWalkInUser.getEmployeeID());
        walkInUserParams.put(API.ManualRegistration.JSON_CONTACT_NUMBER, mWalkInUser.getContactNumber());
        walkInUserParams.put(API.ManualRegistration.JSON_STATUS, mWalkInUser.getStatus());

        params.put(API.ManualRegistration.JSON_EVENT_WALKIN_USER, walkInUserParams);
        params.put(API.ManualRegistration.JSON_EVENT_BATCH_ID, mWorkshop.getId());


        return params;
    }

    private void showDialogAlert(String title, String message){
        mDialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.label_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).create();
        mDialog.show();
    }

    private void enableFormFields(boolean enableFields){

        if(mNameEdit != null &&
            mEmailEdit != null &&
            mEmployeeIDEdit != null &&
            mContactNumberEdit != null &&
            mCheckbox != null &&
            mConfirmButton != null){

            mNameEdit.setEnabled(enableFields);
            mEmailEdit.setEnabled(enableFields);
            mEmployeeIDEdit.setEnabled(enableFields);
            mContactNumberEdit.setEnabled(enableFields);

            mCheckbox.setEnabled(enableFields);
            mConfirmButton.setEnabled(enableFields);
        }

    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class ValidateManualRegistrationTasks extends AsyncTask<Void,Void,Void>
        implements Response.Listener<String>,
            Response.ErrorListener{
        Context context;
        String email, employeeID;
        int eventID;
        String authEmail, authToken;

        StringRequest mRequest;

        public ValidateManualRegistrationTasks(Context context, String email, String employeeID, int eventID, String authEmail, String authToken) {
            this.context = context;
            this.email = email;
            this.employeeID = employeeID;
            this.eventID = eventID;
            this.authEmail = authEmail;
            this.authToken = authToken;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Prepare the parameters. We pass in an empty value for "category" since we want to get all Summit workshops
            String params = String.format(API.ValidateManualRegistration.PARAM_EMAIL + "=%1$s&"
                            + API.ValidateManualRegistration.PARAM_EMPLOYEE_ID + "=%2$s&"
                            + API.ValidateManualRegistration.PARAM_EVENT_BATCH_ID + "=%3$s&"
                            + API.ValidateManualRegistration.PARAM_USER_EMAIL + "=%4$s&"
                            + API.ValidateManualRegistration.PARAM_USER_TOKEN + "=%5$s",
                    email,
                    employeeID,
                    eventID,
                    authEmail,
                    authToken);

            Log.d("~~", "--- params: " + params);

            String url = API.url() + API.ValidateManualRegistration.method() + params;

            Log.d("~~", "--- url: " + url);

            if(mRequest != null){
                mRequest.cancel();
            }

            mRequest = new StringRequest(API.ValidateManualRegistration.httpMethod(), url, this, this);

            VolleyInstance.getInstance(context).addToRequestQueue(mRequest);

            return null;
        }

        @Override
        public void onResponse(String response) {

            Log.i(TAG, "Received response. Parsing JSON");

            try {
                JSONObject jsonObject = new JSONObject(response);

                if(jsonObject != null ){


                    Log.d("~~", "--- response: " + jsonObject);

                    showProgress(false);
                    boolean kumpleto = (jsonObject.has("whitelist") && jsonObject.has("credit") && jsonObject.has("name") &&
                            jsonObject.has("employee_id"));

                    Log.d("~~", "--- kumpeto? " + kumpleto);

                    // Check if the json response has complete fields
                    if(jsonObject.has("whitelist") && jsonObject.has("credit") && jsonObject.has("name") &&
                            jsonObject.has("employee_id")){
                        //disable the form fields first
                        enableFormFields(false);

                        String userName = jsonObject.getString("name");
                        String employeeID = jsonObject.getString("employee_id");
                        int creditRemaining = jsonObject.getInt("credit");
                        String email = mEmailEdit.getText().toString();
                        String contactNumber = mContactNumberEdit.getText().toString();
                        boolean isInWhitelist = jsonObject.getBoolean("whitelist");
                        String comments = "";

                        if(jsonObject.has("comments")) {
                            comments = retrieveJSONComments(jsonObject.getJSONArray("comments"));
                        }

                        String statusMessage = "";

                        showEmployeeDetails(true);

                        mAcceptButton.setEnabled(true);
                        mDeclineButton.setEnabled(true);

                        // Determine the status color and set the status
                            /*
                                Cases:
                                1. Whitelist: FALSE, enough credits - User is eligible: The user is eligible, registered with Summit and has enough credits
                                2. Whitelist:TRUE- User has not created an account: The user is eligible, not registered with Summit but has enough credits
                                3. Whitelist:FALSE, not enough credits- User has insufficient credits: The user is eligible, registered with Summit but does not have enough credits
                             */
                        boolean hasEnoughCredits = creditRemaining - mWorkshop.getCreditValue() >= 0;
                        mStatusText.setTextColor(res.getColor(R.color.textColorCorrect));
                        // Determine the status message
                        String status = "";

                        if(isInWhitelist){
                            //Case 2
                            status = res.getString(R.string.label_user_no_account);
                            statusMessage = WalkInUser.STATUS_NO_ACCOUNT;
                        }else if(hasEnoughCredits){
                            //Case 1
                            status = res.getString(R.string.label_user_eligible);
                            statusMessage = WalkInUser.STATUS_ELIGIBLE;
                        }else{
                            // Case 3
                            status = res.getString(R.string.label_user_insufficient_credits);
                            statusMessage = WalkInUser.STATUS_INSUFFICIENT_CREDITS;
                        }
                        mStatusText.setText(status);

                        mNameText.setText(userName);
                        mCreditsText.setText(String.valueOf(creditRemaining));
                        mCreditCostText.setText(String.valueOf(mWorkshop.getCreditValue()));
                        mCommentsText.setText(comments);

                        // Check if any of the WalkInUser fields are empty
                        // and fill them up based on the input fields
                        if(userName.isEmpty()){
                            userName = mNameEdit.getText().toString();
                        }
                        if(employeeID.isEmpty()){
                            employeeID = mEmployeeIDEdit.getText().toString();
                        }

                        // Create the walkInUser object too
                        mWalkInUser = new WalkInUser(userName,
                                employeeID,
                                creditRemaining,
                                email,
                                contactNumber,
                                isInWhitelist,
                                comments,
                                statusMessage);
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
                showProgress(false);
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            VolleyLog.d(TAG, "Error: " + error);
            showProgress(false);

            String body = "";

            //get response body and parse with appropriate encoding
            if(error.networkResponse.data!=null) {
                try {
                    body = new String(error.networkResponse.data,"UTF-8");

                    JSONObject errorMessage = new JSONObject(body);

                    if(errorMessage.has("errors") && !errorMessage.getString("errors").isEmpty()){
                        //showDialogAlert(res.getString(R.string.label_error), errorMessage.getString("errors"));

                        //enable the form fields first and show employee details
                        enableFormFields(true);
                        showEmployeeDetails(true);

                        mAcceptButton.setEnabled(false);
                        mDeclineButton.setEnabled(false);

                        // Get the status message and set the color to the incorrect color
                        String statusMessage = errorMessage.getString("errors");
                        mStatusText.setTextColor(res.getColor(R.color.textColorIncorrect));
                        mStatusText.setText(statusMessage);
                    }else if(errorMessage.has("base") && !errorMessage.getString("base").isEmpty()){
                        //showDialogAlert(res.getString(R.string.label_error), errorMessage.getString("base"));

                        //enable the form fields first and show employee details
                        enableFormFields(true);
                        showEmployeeDetails(true);

                        mAcceptButton.setEnabled(false);
                        mDeclineButton.setEnabled(false);

                        // Get the status message and set the color to the incorrect color
                        String statusMessage = errorMessage.getString("base");
                        mStatusText.setTextColor(res.getColor(R.color.textColorIncorrect));
                        mStatusText.setText(statusMessage);
                    }else{
                        enableFormFields(true);
                        showEmployeeDetails(false);

                        // Generic error
                        showDialogAlert(res.getString(R.string.label_error), res.getString(R.string.error_generic_try_again));
                    }
                } catch (JSONException | UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class AcceptUserTask extends AsyncTask<Void,Void,Void>
            implements Response.Listener<JSONObject>,
            Response.ErrorListener{
        Context context;
        String email, authToken;
        JsonObjectRequest mCheckInRequest;

        public AcceptUserTask(Context context, String email, String authToken) {
            this.context = context;
            this.email = email;
            this.authToken = authToken;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Prepare the parameters.
            String params = String.format(API.ManualRegistration.PARAM_USER_EMAIL + "=%1$s&"
                            + API.ManualRegistration.PARAM_USER_TOKEN + "=%2$s",
                    email,
                    authToken);

            Log.d("~~", "--- params: " + params);

            String url = API.url() + API.ManualRegistration.method() + params;

            JSONObject jsonParams = null;
            try {
                jsonParams = generateJSONParams();
            } catch (JSONException e) {
                e.printStackTrace();
            }


            Log.d("~~", "--- jsonParams: " + jsonParams);

            if(mCheckInRequest != null){
                mCheckInRequest.cancel();
            }

            mCheckInRequest = new JsonObjectRequest(API.ManualRegistration.httpMethod(), url, jsonParams, this, this) {

                /**
                 * Passing some request headers
                 * */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }

            };

            VolleyInstance.getInstance(context).addToRequestQueue(mCheckInRequest);

            return null;
        }

        @Override
        public void onResponse(JSONObject response) {
            Log.d(TAG, response.toString());
            Log.d("~~", "--- response: " + response);

            setResult(RESULT_OK);
            finish();

            mProgress.setVisibility(View.GONE);
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            VolleyLog.d(TAG, "Error: " + error.getMessage());
            showDialogAlert(res.getString(R.string.label_error), res.getString(R.string.error_generic_try_again));
        }
    }
}
