package com.uobsummit.zpass;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.uobsummit.zpass.constants.API;
import com.uobsummit.zpass.constants.Constants;
import com.uobsummit.zpass.preferences.SecurePreferencesHelper;
import com.uobsummit.zpass.requests.VolleyInstance;
import com.uobsummit.zpass.utils.ZpassUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.Manifest.permission.READ_CONTACTS;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements LoaderCallbacks<Cursor> {

    private static final String TAG = "LoginActivity";

    /**
     * Id to identity READ_CONTACTS permission mRequest.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    // UI references.
    private FrameLayout mLayout;
    private AutoCompleteTextView mEmailView;
    private TextInputEditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;

    // Login request task
    LoginRequestTask mRequestTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //Clear out the auth preferences
        SecurePreferencesHelper.clearAuthPreferences(this);

        mLayout = (FrameLayout) findViewById(R.id.login_layout);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.login_email_textview);
        populateAutoComplete();

        mPasswordView = (TextInputEditText) findViewById(R.id.login_password_textview);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.login_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form_container);
        mProgressView = findViewById(R.id.login_progress);
    }

    @Override
    protected void onResume() {
        super.onResume();

        //TODO debug login. Remove upon release
        if(Constants.isDebug()){
            mEmailView.setText("trainer1@absolute-living.com");
            mPasswordView.setText("password");
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.d("~~", "onNewIntent Login");
    }

    private void populateAutoComplete() {

        if (!mayRequestContacts()) {
            return;
        }

        getLoaderManager().initLoader(0, null, this);
    }

    private boolean mayRequestContacts() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }else{
            // For Marshmallow +
            if (checkSelfPermission(READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                return true;
            }
            if (shouldShowRequestPermissionRationale(READ_CONTACTS)) {
                Snackbar.make(/*mEmailView*/mLayout, R.string.permission_rationale_contacts, Snackbar.LENGTH_INDEFINITE)
                        .setAction(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            @TargetApi(Build.VERSION_CODES.M)
                            public void onClick(View v) {
                                requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
                            }
                        })
                        .show();
            } else {
                requestPermissions(new String[]{READ_CONTACTS}, REQUEST_READ_CONTACTS);
            }
        }
        return false;
    }

    /**
     * Callback received when a permissions mRequest has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_READ_CONTACTS) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                populateAutoComplete();
            }
        }
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        // Close the soft keyboard
        ZpassUtil.hideSoftKeyboard(this);

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(String.format(getString(R.string.error_field_required), getString(R.string.label_email)));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);

            callLoginRequest(email, password);
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }

    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }


    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }



    private void callLoginRequest(final String email, final String password) {

        /*String url = API.url() + API.Login.method();

        // Encode the email/password to Base64
        String credentialsToEncode = email + ":" + password;
        byte[] dataToEncode = new byte[0];
        try {
            dataToEncode = credentialsToEncode.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // Add the encoded email/password as a header
        final HashMap<String, String> headers = new HashMap<String, String>();
        final String authString = "Basic " + Base64.encodeToString(dataToEncode, Base64.DEFAULT);
        headers.put("authorization", authString);

        // Create the Volley request
        mRequest = new StringRequest(API.Login.httpMethod(), url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("~~", "--- response: " + response);

                boolean success = false;

                try {
                    JSONObject jsonResponse = new JSONObject(response);

                    // Retrieve the json result and parse it
                    if(jsonResponse != null ){
                        if(jsonResponse.has("authentication_token")
                                && jsonResponse.has("email")){
                            String password = jsonResponse.getString("authentication_token");
                            String email = jsonResponse.getString("email");

                            boolean areFieldsComplete = !password.isEmpty() && !email.isEmpty();

                            if(areFieldsComplete){
                                //Check to see if the required fields for email and auth token are existing
                                //then save them to Secure preferences.
                                success = SecurePreferencesHelper.storeAuthPreferences(getApplicationContext(), password, email);
                                continueToHome();
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("~~", "Auth preferences saved successfully? " + success);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError: " + error);
                showProgress(false);

                // Authentication failure. Show the user an error message
                if(error instanceof AuthFailureError){
                    mEmailView.setError(getString(R.string.error_incorrect_login));
                    mEmailView.requestFocus();
                }
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };

        VolleyInstance.getInstance(this).addToRequestQueue(mRequest);*/


        String url = API.url() + API.Login.method();

        mRequestTask = new LoginRequestTask(this, url, email, password);
        mRequestTask.execute();
    }

    private void continueToHome(){
        showProgress(false);

        finish();
        Intent intent = new Intent(this, SelectWorkshopActivity.class);
        startActivity(intent);
    }

    private class LoginRequestTask extends AsyncTask<Void, Void, Boolean>{

        Context context;
        String url, email, password;

        // Login mRequest
        StringRequest mRequest;

        public LoginRequestTask(Context context, String url, String email, String password) {
            this.context = context;
            this.url = url;
            this.email = email;
            this.password = password;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            // Encode the email/password to Base64
            String credentialsToEncode = email + ":" + password;
            byte[] dataToEncode = new byte[0];
            try {
                dataToEncode = credentialsToEncode.getBytes("UTF-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }

            // Add the encoded email/password as a header
            final HashMap<String, String> headers = new HashMap<String, String>();
            final String authString = "Basic " + Base64.encodeToString(dataToEncode, Base64.DEFAULT);
            headers.put("authorization", authString);

            // Create the Volley request
            mRequest = new StringRequest(API.Login.httpMethod(), url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    boolean success = false;

                    Log.d("~~", "--- response: " + response);

                    try {
                        JSONObject jsonResponse = new JSONObject(response);

                        // Retrieve the json result and parse it
                        if(jsonResponse != null ){
                            if(jsonResponse.has("authentication_token")
                                    && jsonResponse.has("email")){
                                String authToken = jsonResponse.getString("authentication_token");
                                String email = jsonResponse.getString("email");

                                boolean areFieldsComplete = !authToken.isEmpty() && !email.isEmpty();

                                if(areFieldsComplete){
                                    //Check to see if the required fields for email and auth token are existing
                                    //then save them to Secure preferences.
                                    success = SecurePreferencesHelper.storeAuthPreferences(getApplicationContext(), authToken, email);
                                    continueToHome();
                                }
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Log.d("~~", "Auth preferences saved successfully? " + success);

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "VolleyError: " + error);
                    showProgress(false);

                    // Authentication failure. Show the user an error message
                    if(error instanceof AuthFailureError){
                        mEmailView.setError(getString(R.string.error_incorrect_login));
                        mEmailView.requestFocus();
                    }
                }
            }){
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    return headers;
                }
            };

            VolleyInstance.getInstance(context).addToRequestQueue(mRequest);

            return true;
        }
    }
}

