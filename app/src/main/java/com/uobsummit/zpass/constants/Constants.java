package com.uobsummit.zpass.constants;

import android.content.Context;

import com.uobsummit.zpass.preferences.SecurePreferencesHelper;

/**
 * Created by LloydM on 11/14/16
 * for Livefitter
 */

public class Constants {
    public static final String PREFS_AUTH = "z-passauth";
    private static final boolean DEBUG_MODE = true;

    public static boolean isDebug(){
        return DEBUG_MODE;
    }

    /**
     * Based on authentication preferences, determine if a user is logged in
     * @param context used for secure preferences
     * @return if a user is logged in or not
     */
    public static boolean isLoggedIn(Context context){
        String auth = SecurePreferencesHelper.retrieveAuthToken(context);
        String email = SecurePreferencesHelper.retrieveAuthToken(context);

        return !auth.isEmpty() && !email.isEmpty();
    }
}
