package com.uobsummit.zpass.constants;

import com.android.volley.Request;

/**
 * Created by LloydM on 11/11/16
 * for Livefitter
 */

public class API {
    //public static final String HOST = "https://fitterevent-d.herokuapp.com/";
    public static final String HOST = "https://www.uobsummit.com/";
    public static final String API = "api/v2/";

    public static String url(){
        return HOST + API;
    }

    /**
     * Login API
     */
    public static class Login{
        static final int HTTP_METHOD = Request.Method.POST;
        static final String METHOD_NAME = "admin_users/sessions.json";

        public static int httpMethod() {
            return HTTP_METHOD;
        }
        public static String method() {
            return METHOD_NAME;
        }
    }

    /**
     * Get list of Workshops
     */
    public static class GetListOfWorkshops{
        static final int HTTP_METHOD = Request.Method.GET;
        static final String METHOD_NAME = "events.json?";
        public static final String PARAM_CATEGORY = "category";
        public static final String PARAM_USER_EMAIL = "admin_user_email";
        public static final String PARAM_USER_TOKEN = "admin_user_token";

        public static final String CATEGORY_SKILLSFUTURE = "skillsfuture";

        public static int httpMethod() {
            return HTTP_METHOD;
        }
        public static String method() {
            return METHOD_NAME;
        }
    }

    /**
     * Get pre-registered attendance list
     */
    public static class AttendancePreRegistered{
        static final int HTTP_METHOD = Request.Method.GET;
        static final String METHOD_NAME = "user_events.json?";
        public static final String PARAM_EVENT = "event";
        public static final String PARAM_USER_EMAIL = "admin_user_email";
        public static final String PARAM_USER_TOKEN = "admin_user_token";

        public static int httpMethod() {
            return HTTP_METHOD;
        }
        public static String method() {
            return METHOD_NAME;
        }
    }

    /**
     * Get walk-in attendance list
     */
    public static class AttendanceWalkIn{
        static final int HTTP_METHOD = Request.Method.GET;
        static final String METHOD_NAME = "event_walkin_users.json?";
        public static final String PARAM_EVENT = "event";
        public static final String PARAM_USER_EMAIL = "admin_user_email";
        public static final String PARAM_USER_TOKEN = "admin_user_token";

        public static int httpMethod() {
            return HTTP_METHOD;
        }
        public static String method() {
            return METHOD_NAME;
        }
    }

    /**
     * Validates the sent user ID that was taken from the user's QR code
     */
    public static class ValidateQRCode{
        static final int HTTP_METHOD = Request.Method.GET;
        static final String METHOD_NAME = "user_events.json?";
        public static final String PARAM_USER_ID = "user_id";
        public static final String PARAM_EVENT_ID = "event";
        public static final String PARAM_USER_EMAIL = "admin_user_email";
        public static final String PARAM_USER_TOKEN = "admin_user_token";

        public static int httpMethod() {
            return HTTP_METHOD;
        }
        public static String method() {
            return METHOD_NAME;
        }
    }

    /**
     * Validates the submitted information via manual registration.
     * Only emails that are part of the white-list are counted
     */
    public static class ValidateManualRegistration{
        static final int HTTP_METHOD = Request.Method.GET;
        static final String METHOD_NAME = "users.json?";
        public static final String PARAM_EMAIL = "email";
        public static final String PARAM_EMPLOYEE_ID = "employee_id";
        public static final String PARAM_EVENT_BATCH_ID = "event_batch_id";
        public static final String PARAM_USER_EMAIL = "admin_user_email";
        public static final String PARAM_USER_TOKEN = "admin_user_token";

        public static int httpMethod() {
            return HTTP_METHOD;
        }
        public static String method() {
            return METHOD_NAME;
        }
    }

    /**
     * Submit the user's digital signature
     */
    public static class SubmitDigitalSignature {
        static final int HTTP_METHOD = Request.Method.PUT;
        static final String METHOD_NAME = "user_events/";
        public static final String PARAM_USER_EMAIL = "admin_user_email";
        public static final String PARAM_USER_TOKEN = "admin_user_token";
        public static final String JSON_USER_EVENT = "user_event";
        public static final String JSON_STATUS = "status";
        public static final String JSON_STATUS_VALUE = "checked_in";
        public static final String JSON_DIGITAL_SIGNATURE = "user_event_digital_signature";
        public static final String JSON_SIGNATURE = "signature";

        public static int httpMethod() {
            return HTTP_METHOD;
        }
        public static String method() {
            return METHOD_NAME;
        }
    }

    /**
     * Manual registration for walk-in users who are not part of the system
     * and are not part of the white-list
     */
    public static class ManualRegistration{
        static final int HTTP_METHOD = Request.Method.POST;
        static final String METHOD_NAME = "event_walkin_users.json?";
        public static final String PARAM_USER_EMAIL = "admin_user_email";
        public static final String PARAM_USER_TOKEN = "admin_user_token";
        public static final String JSON_EVENT_BATCH_ID = "event_batch_id";
        public static final String JSON_EVENT_WALKIN_USER = "event_walkin_user";
        public static final String JSON_USER_EMAIL = "email";
        public static final String JSON_EVENT_BATCH_CREDIT = "event_batch_credit_value";
        public static final String JSON_USER_NAME = "name";
        public static final String JSON_EMPLOYEE_ID = "employee_id";
        public static final String JSON_CONTACT_NUMBER = "contact_no";
        public static final String JSON_STATUS = "status";
        public static final String JSON_DIGITAL_SIGNATURE = "user_event_digital_signature";
        public static final String JSON_SIGNATURE = "signature";

        public static int httpMethod() {
            return HTTP_METHOD;
        }
        public static String method() {
            return METHOD_NAME;
        }
    }
}
