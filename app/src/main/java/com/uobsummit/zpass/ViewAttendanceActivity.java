package com.uobsummit.zpass;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.uobsummit.zpass.fragments.BaseAttendanceFragment;
import com.uobsummit.zpass.fragments.PreRegisteredFragment;
import com.uobsummit.zpass.fragments.WalkInsFragment;

public class ViewAttendanceActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    public static final String KEY_EVENT_BATCH_ID = "ViewAttendanceActivity event batch id";

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_attendance);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();

        if(extras != null && extras.containsKey(KEY_EVENT_BATCH_ID)){
            int eventBatchID = extras.getInt(KEY_EVENT_BATCH_ID,-1);

            if(eventBatchID != -1) {
                // Create the adapter that will return a fragment for each of the three
                // primary sections of the activity.
                mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), eventBatchID);

                // Set up the ViewPager with the sections adapter.
                mViewPager = (ViewPager) findViewById(R.id.container);
                mViewPager.setAdapter(mSectionsPagerAdapter);


                TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
                tabLayout.setupWithViewPager(mViewPager);

                mViewPager.addOnPageChangeListener(this);
            }
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_attendance, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_refresh) {
            if(mSectionsPagerAdapter != null && mSectionsPagerAdapter.currentFragment != null){
                mSectionsPagerAdapter.currentFragment.retrieveParticipantsList();
                return true;
            }
        }else if(id == android.R.id.home){
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if(mSectionsPagerAdapter != null){
            mSectionsPagerAdapter.setCurrentFragment(position);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private static final int PAGE_COUNT = 2;
        public BaseAttendanceFragment currentFragment;
        private PreRegisteredFragment preRegisteredFragment;
        private WalkInsFragment walkInFragment;

        public SectionsPagerAdapter(FragmentManager fm, int eventBatchID) {
            super(fm);

            // Set the eventBatchID as arguments for the fragments
            Bundle arguments = new Bundle();
            arguments.putInt(BaseAttendanceFragment.KEY_EVENT_ID, eventBatchID);

            preRegisteredFragment = new PreRegisteredFragment();
            preRegisteredFragment.setArguments(arguments);
            walkInFragment = new WalkInsFragment();
            walkInFragment.setArguments(arguments);

            // Set the currentFragment by default
            currentFragment = preRegisteredFragment;
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0) {
                return preRegisteredFragment;
            }else{
                return walkInFragment;
            }
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        public void setCurrentFragment(int position){
            if(position == 0) {
                currentFragment = preRegisteredFragment;
            }else{
                currentFragment = walkInFragment;
            }

            Log.d("~~", "Current fragment changed: " + currentFragment);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.title_fragment_preregistered);
                case 1:
                    return getString(R.string.title_fragment_walkins);
            }
            return null;
        }
    }
}
