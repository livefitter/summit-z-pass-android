package com.uobsummit.zpass.preferences;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import com.securepreferences.SecurePreferences;
import com.uobsummit.zpass.constants.Constants;

/**
 * Created by LloydM on 11/14/16
 * for Livefitter
 */

public class SecurePreferencesHelper {

    private static final String KEY_AUTH_TOKEN = "z-pass_auth_token";
    private static final String KEY_EMAIL = "z-pass_email";

    public static boolean storeAuthPreferences(Context context, String authToken, String email){
        String authHash = SecurePreferences.hashPrefKey(KEY_AUTH_TOKEN);
        String emailHash = SecurePreferences.hashPrefKey(KEY_EMAIL);

        SharedPreferences prefs = new SecurePreferences(context.getApplicationContext());
        SecurePreferences.setLoggingEnabled(Constants.isDebug());
        SharedPreferences.Editor editor = prefs.edit();

        editor.putString(authHash, authToken);
        editor.putString(emailHash, email);

       return editor.commit();
    }

    public static String retrieveAuthToken(Context context){
        String authHash = SecurePreferences.hashPrefKey(KEY_AUTH_TOKEN);

        SharedPreferences prefs = new SecurePreferences(context.getApplicationContext());
        SecurePreferences.setLoggingEnabled(Constants.isDebug());

        return prefs.getString(authHash, "");
    }

    public static String retrieveEmail(Context context){
        String emailHash = SecurePreferences.hashPrefKey(KEY_EMAIL);

        SharedPreferences prefs = new SecurePreferences(context.getApplicationContext());
        SecurePreferences.setLoggingEnabled(Constants.isDebug());

        return prefs.getString(emailHash, "");
    }

    public static void clearAuthPreferences(Context context){
        String authHash = SecurePreferences.hashPrefKey(KEY_AUTH_TOKEN);
        String emailHash = SecurePreferences.hashPrefKey(KEY_EMAIL);

        SharedPreferences.Editor editor = new SecurePreferences(context.getApplicationContext()).edit();
        editor.remove(authHash);
        editor.remove(emailHash);
        editor.apply();
    }
}
