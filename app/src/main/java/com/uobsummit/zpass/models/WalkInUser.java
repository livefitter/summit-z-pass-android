package com.uobsummit.zpass.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;


/**
 * Created by LloydM on 11/22/16
 * for Livefitter
 */

public class WalkInUser implements Parcelable {

    private String name;
    private String employeeID;
    private int credits;
    private String email;
    private String contactNumber;
    private boolean isInWhiteList;
    private String comments;
    private String status;

    public static final String JSON_NAME = "name";
    public static final String JSON_EMPLOYEE_ID = "employee_id";
    public static final String JSON_CREDIT = "credits";
    public static final String JSON_WHITE_LIST = "whitelist";
    public static final String JSON_COMMENTS = "comments";

    public static final String STATUS_ELIGIBLE = "User is eligible";
    public static final String STATUS_INSUFFICIENT_CREDITS = "User has insufficient credits";
    public static final String STATUS_NO_ACCOUNT = "User has not created an account";

    public WalkInUser(String name, String employeeID, int credits, String email, String contactNumber, boolean isInWhiteList, String comments, String status) {
        this.name = name;
        this.employeeID = employeeID;
        this.credits = credits;
        this.email = email;
        this.contactNumber = contactNumber;
        this.isInWhiteList = isInWhiteList;
        this.comments = comments;
        this.status = status;
    }

    public WalkInUser(Parcel in) {
        readFromParcel(in);
    }

    public static final Creator<WalkInUser> CREATOR = new Creator<WalkInUser>() {
        @Override
        public WalkInUser createFromParcel(Parcel in) {
            return new WalkInUser(in);
        }

        @Override
        public WalkInUser[] newArray(int size) {
            return new WalkInUser[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in){
        this.name = in.readString();
        this.employeeID = in.readString();
        this.credits = in.readInt();
        this.email = in.readString();
        this.contactNumber = in.readString();
        this.isInWhiteList = in.readInt() != 0;
        this.comments = in.readString();
        this.status = in.readString();
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(name);
        out.writeString(employeeID);
        out.writeInt(credits);
        out.writeString(email);
        out.writeString(contactNumber);
        out.writeInt(isInWhiteList? 1 : 0);
        out.writeString(comments);
        out.writeString(status);
    }

    public String getName() {
        return name;
    }

    public String getEmployeeID() {
        return employeeID;
    }

    public int getCredits() {
        return credits;
    }

    public String getEmail() {
        return email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public boolean isInWhiteList() {
        return isInWhiteList;
    }

    public String getComments() {
        return comments;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        String fieldDeclarationString = this.getClass().getSimpleName() + "{ ";
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field f : fields){
            try {
                if(f.getModifiers() == Modifier.PRIVATE && !f.getName().equals("CREATOR")){
                    fieldDeclarationString += f.getName() + ": " + f.get(this) + ", \n";
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        fieldDeclarationString += "}";

        return fieldDeclarationString;
    }
}
