package com.uobsummit.zpass.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by LloydM on 7/9/16
 * for Livefitter
 */
//TODO probably outdated. Remove
public class ZalentsEvent implements Parcelable {

//            "approved_at": "2016-06-20T22:29:23+08:00",
//            "category_list": [
//            "Landscape"
//            ],
//            "company_id": 4,
//            "created_at": "2016-06-10T08:36:01+08:00",
//            "created_by_id": null,
//            "credit": 0.0,
//            "description": "",
//            "draft_id": null,
//            "html_description": "<p><strong>This is NOT the description.&nbsp;</strong></p>\r\n<p>&nbsp;</p>",
//            "html_instructor": "<p><em>This is the instructor's description.</em></p>",
//            "id": 7,
//            "image_large_url": "",
//            "image_library_id": 17,
//            "image_medium_url": "",
//            "image_small_url": "",
//            "key_takeaway": "",
//            "modified_by_id": null,
//            "organizer_information": "Gain this and that",
//            "prerequisite": "No requirements",
//            "preview_description": "This is the subtitle",
//            "published_at": "2016-06-20T22:30:28+08:00",
//            "status": "Published",
//            "submitted_at": "2016-06-20T22:29:09+08:00",
//            "title": "This is the title",
//            "trashed_at": null,
//            "updated_at": "2016-06-13T22:44:31+08:00",
//            "event_batches": []

    private String approvedAtDate;
    private ArrayList<String> categoryList;
    private int companyID;
    private String createdAtDate;
    private int createdByID;
    private float credits;
    private String description;
    private int draftID;
    private String htmlDescription;
    private String htmlInstructor;
    private int id;
    private String imageLargeURL;
    private String imageMediumURL;
    private String imageSmallURL;
    private int imageLibraryID;
    private String keyTakeaway;
    private int modifiedByID;
    private String organizerInformation;
    private String prerequisite;
    private String previewDescription;
    private String publishedAtDate;
    private String status;
    private String submittedAtDate;
    private String title;
    private String trashedAtDate;
    private String updatedAtDate;
    private ArrayList<EventBatch> eventBatches;

    public ZalentsEvent(int id,
                        String title,
                        String description,
                        float credits,
                        String imageLargeURL,
                        String imageMediumURL,
                        String imageSmallURL,
                        ArrayList<EventBatch> eventBatches){
        this.id = id;
        this.title = title;
        this.description = description;
        this.credits = credits;
        this.imageLargeURL = imageLargeURL;
        this.imageMediumURL = imageMediumURL;
        this.imageSmallURL = imageSmallURL;
        this.eventBatches = eventBatches;
    }

    public ZalentsEvent(String approvedAtDate, ArrayList<String> categoryList, int companyID, String createdAtDate, int createdByID, float credits, String description, int draftID, String htmlDescription, String htmlInstructor, int id, String imageLargeURL, String imageMediumURL, String imageSmallURL, int imageLibraryID, String keyTakeaway, int modifiedByID, String organizerInformation, String prerequisite, String previewDescription, String publishedAtDate, String status, String submittedAtDate, String title, String trashedAtDate, String updatedAtDate, ArrayList<EventBatch> eventBatches) {
        this.approvedAtDate = approvedAtDate;
        this.categoryList = categoryList;
        this.companyID = companyID;
        this.createdAtDate = createdAtDate;
        this.createdByID = createdByID;
        this.credits = credits;
        this.description = description;
        this.draftID = draftID;
        this.htmlDescription = htmlDescription;
        this.htmlInstructor = htmlInstructor;
        this.id = id;
        this.imageLargeURL = imageLargeURL;
        this.imageMediumURL = imageMediumURL;
        this.imageSmallURL = imageSmallURL;
        this.imageLibraryID = imageLibraryID;
        this.keyTakeaway = keyTakeaway;
        this.modifiedByID = modifiedByID;
        this.organizerInformation = organizerInformation;
        this.prerequisite = prerequisite;
        this.previewDescription = previewDescription;
        this.publishedAtDate = publishedAtDate;
        this.status = status;
        this.submittedAtDate = submittedAtDate;
        this.title = title;
        this.trashedAtDate = trashedAtDate;
        this.updatedAtDate = updatedAtDate;
        this.eventBatches = eventBatches;
    }

    public ZalentsEvent(Parcel parc){
        readFromParcel(parc);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public void readFromParcel(Parcel in){

        approvedAtDate = in.readString();
        categoryList = in.readArrayList(String.class.getClassLoader());
        companyID = in.readInt();
        createdAtDate = in.readString();
        createdByID = in.readInt();
        credits = in.readFloat();
        description = in.readString();
        draftID = in.readInt();
        htmlDescription = in.readString();
        htmlInstructor = in.readString();
        id = in.readInt();
        imageLargeURL = in.readString();
        imageMediumURL = in.readString();
        imageSmallURL = in.readString();
        imageLibraryID = in.readInt();
        keyTakeaway = in.readString();
        modifiedByID = in.readInt();
        organizerInformation = in.readString();
        prerequisite = in.readString();
        previewDescription = in.readString();
        publishedAtDate = in.readString();
        status = in.readString();
        submittedAtDate = in.readString();
        title = in.readString();
        trashedAtDate = in.readString();
        updatedAtDate = in.readString();
        eventBatches = in.readArrayList(EventBatch.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {

        out.writeString(approvedAtDate);
        out.writeStringList(categoryList);
        out.writeInt(companyID);
        out.writeString(createdAtDate);
        out.writeInt(createdByID);
        out.writeFloat(credits);
        out.writeString(description);
        out.writeInt(draftID);
        out.writeString(htmlDescription);
        out.writeString(htmlInstructor);
        out.writeInt(id);
        out.writeString(imageLargeURL);
        out.writeString(imageMediumURL);
        out.writeString(imageSmallURL);
        out.writeInt(imageLibraryID);
        out.writeString(keyTakeaway);
        out.writeInt(modifiedByID);
        out.writeString(organizerInformation);
        out.writeString(prerequisite);
        out.writeString(previewDescription);
        out.writeString(publishedAtDate);
        out.writeString(status);
        out.writeString(submittedAtDate);
        out.writeString(title);
        out.writeString(trashedAtDate);
        out.writeString(updatedAtDate);
        out.writeList(eventBatches);
    }

    public static final Parcelable.Creator<ZalentsEvent> CREATOR
            = new Parcelable.Creator<ZalentsEvent>() {
        public ZalentsEvent createFromParcel(Parcel in) {
            return new ZalentsEvent(in);
        }

        public ZalentsEvent[] newArray(int size) {
            return new ZalentsEvent[size];
        }
    };

    // Getters/Setters
    public int getCreatedByID() {
        return createdByID;
    }

    public void setCreatedByID(int createdByID) {
        this.createdByID = createdByID;
    }

    public String getCreatedAtDate() {
        return createdAtDate;
    }

    public void setCreatedAtDate(String createdAtDate) {
        this.createdAtDate = createdAtDate;
    }

    public String getApprovedAtDate() {
        return approvedAtDate;
    }

    public void setApprovedAtDate(String approvedAtDate) {
        this.approvedAtDate = approvedAtDate;
    }

    public ArrayList<String> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(ArrayList<String> categoryList) {
        this.categoryList = categoryList;
    }

    public int getCompanyID() {
        return companyID;
    }

    public void setCompanyID(int companyID) {
        this.companyID = companyID;
    }

    public float getCredits() {
        return credits;
    }

    public void setCredits(float credits) {
        this.credits = credits;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDraftID() {
        return draftID;
    }

    public void setDraftID(int draftID) {
        this.draftID = draftID;
    }

    public String getHtmlDescription() {
        return htmlDescription;
    }

    public void setHtmlDescription(String htmlDescription) {
        this.htmlDescription = htmlDescription;
    }

    public String getHtmlInstructor() {
        return htmlInstructor;
    }

    public void setHtmlInstructor(String htmlInstructor) {
        this.htmlInstructor = htmlInstructor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImageLargeURL() {
        return imageLargeURL;
    }

    public void setImageLargeURL(String imageLargeURL) {
        this.imageLargeURL = imageLargeURL;
    }

    public String getImageMediumURL() {
        return imageMediumURL;
    }

    public void setImageMediumURL(String imageMediumURL) {
        this.imageMediumURL = imageMediumURL;
    }

    public String getImageSmallURL() {
        return imageSmallURL;
    }

    public void setImageSmallURL(String imageSmallURL) {
        this.imageSmallURL = imageSmallURL;
    }

    public int getImageLibraryID() {
        return imageLibraryID;
    }

    public void setImageLibraryID(int imageLibraryID) {
        this.imageLibraryID = imageLibraryID;
    }

    public String getKeyTakeaway() {
        return keyTakeaway;
    }

    public void setKeyTakeaway(String keyTakeaway) {
        this.keyTakeaway = keyTakeaway;
    }

    public int getModifiedByID() {
        return modifiedByID;
    }

    public void setModifiedByID(int modifiedByID) {
        this.modifiedByID = modifiedByID;
    }

    public String getOrganizerInformation() {
        return organizerInformation;
    }

    public void setOrganizerInformation(String organizerInformation) {
        this.organizerInformation = organizerInformation;
    }

    public String getPrerequisite() {
        return prerequisite;
    }

    public void setPrerequisite(String prerequisite) {
        this.prerequisite = prerequisite;
    }

    public String getPreviewDescription() {
        return previewDescription;
    }

    public void setPreviewDescription(String previewDescription) {
        this.previewDescription = previewDescription;
    }

    public String getPublishedAtDate() {
        return publishedAtDate;
    }

    public void setPublishedAtDate(String publishedAtDate) {
        this.publishedAtDate = publishedAtDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubmittedAtDate() {
        return submittedAtDate;
    }

    public void setSubmittedAtDate(String submittedAtDate) {
        this.submittedAtDate = submittedAtDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTrashedAtDate() {
        return trashedAtDate;
    }

    public void setTrashedAtDate(String trashedAtDate) {
        this.trashedAtDate = trashedAtDate;
    }

    public String getUpdatedAtDate() {
        return updatedAtDate;
    }

    public void setUpdatedAtDate(String updatedAtDate) {
        this.updatedAtDate = updatedAtDate;
    }

    public ArrayList<EventBatch> getEventBatches() {
        return eventBatches;
    }

    public void setEventBatches(ArrayList<EventBatch> eventBatches) {
        this.eventBatches = eventBatches;
    }
}
