package com.uobsummit.zpass.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by LloydM on 7/9/16
 * for Livefitter
 */
//TODO probably outdated. Remove
public class EventBatch implements Parcelable{

//    created_at": "2016-06-10T08:41:37+08:00",
//    "credit": 0.0,
//    "duration": 8.5,
//    "end_time": "17:30",
//    "ended_at": "2016-05-13T08:00:00+08:00",
//    "event_id": 7,
//    "id": 9,
//    "latitude": null,
//    "longitude": null,
//    "slot": 50,
//    "start_time": "09:00",
//    "started_at": "2016-05-12T08:00:00+08:00",
//    "status": "Active",
//    "title": null,
//    "updated_at": "2016-06-20T22:29:09+08:00",
//    "venue": null,
//    "venue_1": "7 Jalan Kilang",
//    "venue_2": "Singapore 159407",
//    "user_events": [ ]

    private String createdAtDate;
    private float credit;
    private float duration;
    private String endTime;
    private String endedAtDate;
    private int eventID; //which event this event batch belongs to
    private int id;
    private float latitude;
    private float longitude;
    private int slots;
    private String startTime;
    private String startedAtDate;
    private String status;
    private String title;
    private String updatedAtDate;
    private ArrayList<String> venue = new ArrayList<>();
    private ArrayList<UserEvent> userEvents = new ArrayList<>();

    // Added fields from parent ZalentsEvent.
    private String eventDescription;
    private String eventHTMLDescription;
    private String eventImageLargeURL;
    private String eventImageMediumURL;
    private String eventImageSmallURL;
    private String eventTitle;

    // Added temporary field for dummy images
    private int tempImageRes;

    public EventBatch() {

    }

    public EventBatch(Parcel parc){
        readFromParcel(parc);
    }

    public void readFromParcel(Parcel in){
        createdAtDate = in.readString();
        credit = in.readFloat();
        duration = in.readFloat();
        endTime = in.readString();
        endedAtDate = in.readString();
        eventID = in.readInt();
        id = in.readInt();
        latitude = in.readFloat();
        longitude = in.readFloat();
        slots = in.readInt();
        startTime = in.readString();
        startedAtDate = in.readString();
        status = in.readString();
        title = in.readString();
        updatedAtDate = in.readString();
        in.readStringList(venue);
        userEvents = in.readArrayList(UserEvent.class.getClassLoader());

        eventDescription = in.readString();
        eventHTMLDescription = in.readString();
        eventImageLargeURL = in.readString();
        eventImageMediumURL = in.readString();
        eventImageSmallURL = in.readString();
        eventTitle = in.readString();

        // Temporary field. remove when done prototyping
        tempImageRes = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(createdAtDate);
        out.writeFloat(credit);
        out.writeFloat(duration);
        out.writeString(endTime);
        out.writeString(endedAtDate);
        out.writeInt(eventID);
        out.writeInt(id);
        out.writeFloat(latitude);
        out.writeFloat(longitude);
        out.writeInt(slots);
        out.writeString(startTime);
        out.writeString(startedAtDate);
        out.writeString(status);
        out.writeString(title);
        out.writeString(updatedAtDate);
        out.writeStringList(venue);
        out.writeList(userEvents);

        out.writeString(eventDescription);
        out.writeString(eventHTMLDescription);
        out.writeString(eventImageLargeURL);
        out.writeString(eventImageMediumURL);
        out.writeString(eventImageSmallURL);
        out.writeString(eventTitle);

        // Temporary field. remove when done prototyping
        out.writeInt(tempImageRes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<EventBatch> CREATOR
            = new Parcelable.Creator<EventBatch>() {
        public EventBatch createFromParcel(Parcel in) {
            return new EventBatch(in);
        }

        public EventBatch[] newArray(int size) {
            return new EventBatch[size];
        }
    };

    public String getCreatedAtDate() {
        return createdAtDate;
    }

    public void setCreatedAtDate(String createdAtDate) {
        this.createdAtDate = createdAtDate;
    }

    public float getCredit() {
        return credit;
    }

    public void setCredit(float credit) {
        this.credit = credit;
    }

    public float getDuration() {
        return duration;
    }

    public void setDuration(float duration) {
        this.duration = duration;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEndedAtDate() {
        return endedAtDate;
    }

    public void setEndedAtDate(String endedAtDate) {
        this.endedAtDate = endedAtDate;
    }

    public int getEventID() {
        return eventID;
    }

    public void setEventID(int eventID) {
        this.eventID = eventID;
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public int getSlots() {
        return slots;
    }

    public void setSlots(int slots) {
        this.slots = slots;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStartedAtDate() {
        return startedAtDate;
    }

    public void setStartedAtDate(String startedAtDate) {
        this.startedAtDate = startedAtDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUpdatedAtDate() {
        return updatedAtDate;
    }

    public void setUpdatedAtDate(String updatedAtDate) {
        this.updatedAtDate = updatedAtDate;
    }

    public ArrayList<String> getVenue() {
        return venue;
    }

    public void setVenue(ArrayList<String> venue) {
        this.venue = venue;
    }

    public ArrayList<UserEvent> getUserEvents() {
        return userEvents;
    }

    public void setUserEvents(ArrayList<UserEvent> userEvents) {
        this.userEvents = userEvents;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventHTMLDescription() {
        return eventHTMLDescription;
    }

    public void setEventHTMLDescription(String eventHTMLDescription) {
        this.eventHTMLDescription = eventHTMLDescription;
    }

    public String getEventImageLargeURL() {
        return eventImageLargeURL;
    }

    public void setEventImageLargeURL(String eventImageLargeURL) {
        this.eventImageLargeURL = eventImageLargeURL;
    }

    public String getEventImageMediumURL() {
        return eventImageMediumURL;
    }

    public void setEventImageMediumURL(String eventImageMediumURL) {
        this.eventImageMediumURL = eventImageMediumURL;
    }

    public String getEventImageSmallURL() {
        return eventImageSmallURL;
    }

    public void setEventImageSmallURL(String eventImageSmallURL) {
        this.eventImageSmallURL = eventImageSmallURL;
    }

    public String getEventTitle() {
        return eventTitle;
    }

    public void setEventTitle(String eventTitle) {
        this.eventTitle = eventTitle;
    }

    public int getTempImageRes() {
        return tempImageRes;
    }

    public void setTempImageRes(int tempImageRes) {
        this.tempImageRes = tempImageRes;
    }

    @Override
    public String toString() {
        String fieldDeclarationString = this.getClass().getSimpleName() + "{ ";
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field f : fields){
            try {

                if(f.get(this) != null && !f.getName().equals("CREATOR")){
                    fieldDeclarationString += f.getName() + ": " + f.get(this) + ", \n";
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        fieldDeclarationString += "}";

        return fieldDeclarationString;
    }
}
