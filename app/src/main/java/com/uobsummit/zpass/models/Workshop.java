package com.uobsummit.zpass.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.uobsummit.zpass.utils.ZpassUtil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;


/**
 * Created by LloydM on 11/15/16
 * for Livefitter
 */

public class Workshop implements Parcelable {

    private int id;
    private int category;
    private String title;
    private String session;
    private String schedule;
    private int participants;
    private int participantsCheckedIn;
    private int participantsWalkIn;
    private int creditValue;

    // JSON KEYS
    public static final String JSON_ID = "id";
    public static final String JSON_CATEGORY = "category";
    public static final String JSON_TITLE = "title";
    public static final String JSON_SESSION = "session";
    public static final String JSON_SCHEDULE = "schedule";
    public static final String JSON_PARTICIPANTS_CHECKIN = "checked_in_participants";
    public static final String JSON_PARTICIPANTS = "participants";
    public static final String JSON_PARTICIPANTS_WALKIN = "walkin_participants";
    public static final String JSON_CREDIT_VALUE = "event_credit_value";

    // Category id which determines if a workshop is a core workshop or not
    public static final int CORE_CATEGORY_ID = 1;

    public Workshop(int id, int category, String title, String session, String schedule, int participants, int participantsCheckedIn, int participantsWalkIn, int creditValue) {
        this.id = id;
        this.category = category;
        this.title = title;
        this.session = session;
        this.schedule = schedule;
        this.participants = participants;
        this.participantsCheckedIn = participantsCheckedIn;
        this.participantsWalkIn = participantsWalkIn;
        this.creditValue = creditValue;
    }

    protected Workshop(Parcel in) {
        readFromParcel(in);
    }

    public static final Creator<Workshop> CREATOR = new Creator<Workshop>() {
        @Override
        public Workshop createFromParcel(Parcel in) {
            return new Workshop(in);
        }

        @Override
        public Workshop[] newArray(int size) {
            return new Workshop[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in){
        id = in.readInt();
        category = in.readInt();
        title = in.readString();
        session = in.readString();
        schedule = in.readString();
        participants = in.readInt();
        participantsCheckedIn = in.readInt();
        participantsWalkIn = in.readInt();
        creditValue = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(category);
        parcel.writeString(title);
        parcel.writeString(session);
        parcel.writeString(schedule);
        parcel.writeInt(participants);
        parcel.writeInt(participantsCheckedIn);
        parcel.writeInt(participantsWalkIn);
        parcel.writeInt(creditValue);
    }

    public int getId() {
        return id;
    }

    public int getCategory() {
        return category;
    }

    public String getTitle() {
        return title;
    }

    public String getSession() {
        return session;
    }

    public String getSchedule() {
        return schedule;
    }

    public int getParticipants() {
        return participants;
    }

    public int getParticipantsCheckedIn() {
        return participantsCheckedIn;
    }

    public void setParticipantsCheckedIn(int participantsCheckedIn) {
        this.participantsCheckedIn = participantsCheckedIn;
    }

    public int getParticipantsWalkIn() {
        return participantsWalkIn;
    }

    public int getCreditValue() {
        return creditValue;
    }


    public boolean isCoreWorkshop(){
        return category == CORE_CATEGORY_ID;
    }

    @Override
    public String toString() {
        String fieldDeclarationString = this.getClass().getSimpleName() + "{ ";
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field f : fields){
            try {
                if(f.getModifiers() == Modifier.PRIVATE && !f.getName().equals("CREATOR")){
                    fieldDeclarationString += f.getName() + ": " + f.get(this) + ", \n";
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        fieldDeclarationString += "}";

        return fieldDeclarationString;
    }
}
