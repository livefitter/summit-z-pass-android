package com.uobsummit.zpass.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by LloydM on 7/13/16
 * for Livefitter
 */
//TODO probably outdated. Remove
public class UserEvent implements Parcelable {

//    booked_at": "2016-06-14T23:32:56+08:00",
//    "confirmed_at": null,
//    "created_at": "2016-06-14T23:32:56+08:00",
//    "event_batch_id": 10,
//    "id": 8,
//    "status": "booked",
//    "updated_at": "2016-06-14T23:32:56+08:00",
//    "user_id": 8,
//    "withdrawn_at": null,
//    "user": {}

    private String bookedAtDate;
    private String confirmedAtDate;
    private String createdAtDate;
    private int eventBatchID; //which event batch this user event belongs to
    private int id;
    private String status;
    private String updatedAtDate;
    private int userID;
    private String withdrawnAtDate;
    private ZalentsUser user;

    public UserEvent(){

    }

    public UserEvent(String bookedAtDate, String confirmedAtDate, String createdAtDate, int eventBatchID, int id, String status, String updatedAtDate, int userID, String withdrawnAtDate, ZalentsUser user) {
        this.bookedAtDate = bookedAtDate;
        this.confirmedAtDate = confirmedAtDate;
        this.createdAtDate = createdAtDate;
        this.eventBatchID = eventBatchID;
        this.id = id;
        this.status = status;
        this.updatedAtDate = updatedAtDate;
        this.userID = userID;
        this.withdrawnAtDate = withdrawnAtDate;
        this.user = user;
    }

    public UserEvent(Parcel parc){
        readFromParcel(parc);
    }

    public void readFromParcel(Parcel in){
        bookedAtDate = in.readString();
        confirmedAtDate = in.readString();
        createdAtDate = in.readString();
        eventBatchID = in.readInt();
        id = in.readInt();
        status = in.readString();
        updatedAtDate = in.readString();
        userID = in.readInt();
        withdrawnAtDate = in.readString();
        user = in.readParcelable(ZalentsUser.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(bookedAtDate);
        out.writeString(confirmedAtDate);
        out.writeString(createdAtDate);
        out.writeInt(eventBatchID);
        out.writeInt(id);
        out.writeString(status);
        out.writeString(updatedAtDate);
        out.writeInt(userID);
        out.writeString(withdrawnAtDate);
        out.writeParcelable(user, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<UserEvent> CREATOR
            = new Parcelable.Creator<UserEvent>() {
        public UserEvent createFromParcel(Parcel in) {
            return new UserEvent(in);
        }

        public UserEvent[] newArray(int size) {
            return new UserEvent[size];
        }
    };

    public String getBookedAtDate() {
        return bookedAtDate;
    }

    public void setBookedAtDate(String bookedAtDate) {
        this.bookedAtDate = bookedAtDate;
    }

    public String getConfirmedAtDate() {
        return confirmedAtDate;
    }

    public void setConfirmedAtDate(String confirmedAtDate) {
        this.confirmedAtDate = confirmedAtDate;
    }

    public String getCreatedAtDate() {
        return createdAtDate;
    }

    public void setCreatedAtDate(String createdAtDate) {
        this.createdAtDate = createdAtDate;
    }

    public int getEventBatchID() {
        return eventBatchID;
    }

    public void setEventBatchID(int eventBatchID) {
        this.eventBatchID = eventBatchID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUpdatedAtDate() {
        return updatedAtDate;
    }

    public void setUpdatedAtDate(String updatedAtDate) {
        this.updatedAtDate = updatedAtDate;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getWithdrawnAtDate() {
        return withdrawnAtDate;
    }

    public void setWithdrawnAtDate(String withdrawnAtDate) {
        this.withdrawnAtDate = withdrawnAtDate;
    }

    public ZalentsUser getUser() {
        return user;
    }

    public void setUser(ZalentsUser user) {
        this.user = user;
    }
}
