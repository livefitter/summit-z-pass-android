package com.uobsummit.zpass.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by LloydM on 7/13/16
 * for Livefitter
 */
//TODO probably outdated. Remove
public class ZalentsUser implements Parcelable{


//    "first_name": "C",
//    "id": 8,
//    "last_name": "A"

    private String firstName;
    private int id;
    private String lastName;

    public ZalentsUser(String firstName, int id, String lastName) {
        this.firstName = firstName;
        this.id = id;
        this.lastName = lastName;
    }

    public ZalentsUser(Parcel parc){
        readFromParcel(parc);
    }

    public void readFromParcel(Parcel in){
        firstName = in.readString();
        id = in.readInt();
        lastName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(firstName);
        out.writeInt(id);
        out.writeString(lastName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Parcelable.Creator<ZalentsUser> CREATOR
            = new Parcelable.Creator<ZalentsUser>() {
        public ZalentsUser createFromParcel(Parcel in) {
            return new ZalentsUser(in);
        }

        public ZalentsUser[] newArray(int size) {
            return new ZalentsUser[size];
        }
    };

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
