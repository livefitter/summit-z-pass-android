package com.uobsummit.zpass.requests;

import android.util.Log;

import com.uobsummit.zpass.models.Workshop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;

/**
 * Created by LloydM on 11/16/16
 * for Livefitter
 *
 * This class parses a given JSONArray object and returns a list of Workshop objects for use
 */
public class WorkshopListParser {

    private static final String TAG = "WorkshopListParser";

    public static ArrayList<Workshop> parseJSON(JSONArray jsonWorkshopList) throws JSONException {
        ArrayList<Workshop> workshopList = new ArrayList<Workshop>();

        for (int i = 0; i < jsonWorkshopList.length(); i++){
            JSONObject jsonWorkshop = jsonWorkshopList.getJSONObject(i);

            if(jsonWorkshop != null){
                // determine if this particular workshop entry is complete
                // otherwise, ignore this entry
                if (!jsonWorkshop.has(Workshop.JSON_ID)
                        && !jsonWorkshop.has(Workshop.JSON_CATEGORY)
                        && !jsonWorkshop.has(Workshop.JSON_TITLE)
                        && !jsonWorkshop.has(Workshop.JSON_SESSION)
                        && !jsonWorkshop.has(Workshop.JSON_SCHEDULE)
                        && !jsonWorkshop.has(Workshop.JSON_PARTICIPANTS_CHECKIN)
                        && !jsonWorkshop.has(Workshop.JSON_PARTICIPANTS)
                        && !jsonWorkshop.has(Workshop.JSON_PARTICIPANTS_WALKIN)
                        && !jsonWorkshop.has(Workshop.JSON_CREDIT_VALUE)) {
                    Log.d(TAG, "Incomplete entry at " + i + ": " + jsonWorkshop);
                    continue;
                }

                // Retrieve the values from JSON
                int id = jsonWorkshop.getInt(Workshop.JSON_ID);
                int category = jsonWorkshop.getInt(Workshop.JSON_CATEGORY);
                String title = jsonWorkshop.getString(Workshop.JSON_TITLE);
                String session = jsonWorkshop.getString(Workshop.JSON_SESSION);
                String schedule = jsonWorkshop.getString(Workshop.JSON_SCHEDULE);
                int participants = jsonWorkshop.getInt(Workshop.JSON_PARTICIPANTS);
                int participantsCheckedIn = jsonWorkshop.getInt(Workshop.JSON_PARTICIPANTS_CHECKIN);
                int participantsWalkIn = jsonWorkshop.getInt(Workshop.JSON_PARTICIPANTS_WALKIN);
                int creditValue = jsonWorkshop.getInt(Workshop.JSON_CREDIT_VALUE);

                // and create a new Workshop instance out of it
                Workshop workshop = new Workshop(id,
                        category,
                        title,
                        session,
                        schedule,
                        participants,
                        participantsCheckedIn,
                        participantsWalkIn,
                        creditValue);

                workshopList.add(workshop);
            }
        }

        return workshopList;
    }
}
