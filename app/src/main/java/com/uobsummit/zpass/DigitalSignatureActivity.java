package com.uobsummit.zpass;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.uobsummit.zpass.constants.API;
import com.uobsummit.zpass.models.WalkInUser;
import com.uobsummit.zpass.models.Workshop;
import com.uobsummit.zpass.preferences.SecurePreferencesHelper;
import com.uobsummit.zpass.requests.VolleyInstance;
import com.uobsummit.zpass.views.DrawingView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by LloydM on 11/17/16
 * for Livefitter
 */

public class DigitalSignatureActivity extends AppCompatActivity
    implements View.OnClickListener,
        Response.ErrorListener,
        Response.Listener<JSONObject>{

    public static final int MODE_QR_SCAN = 0x153;
    public static final int MODE_MANUAL_REGISTRATION = 0x154;
    /**
     * Internal flag that determines which API to call and which methods to execute
     * only valid values are MODE_QR_SCAN or MODE_MANUAL_REGISTRATION
     */
    private int mRegistrationMode = -1;

    private static final String TAG = "DigitalSignature";

    public static final String KEY_REGISTRATION_MODE = "registration mode";
    public static final String KEY_USER_EVENT_ID = "user_event_id value";
    public static final String KEY_WORKSHOP = "workshop value";
    public static final String KEY_WALK_IN_USER = "walk in user value";

    DrawingView mDrawingView;
    Button mClearButton, mSubmitButton;
    FrameLayout mProgressView;

    // Required fields to use for API calls
    int mUserEventID = -1;
    Workshop mWorkshop;
    WalkInUser mWalkInUser;

    // API request
    JsonObjectRequest mRequest;
    Bitmap mSignatureBitmap;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_digital_signature);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            // Check the mode first
            mRegistrationMode = extras.getInt(KEY_REGISTRATION_MODE, -1);

            if(mRegistrationMode == MODE_QR_SCAN){
                mUserEventID = extras.getInt(KEY_USER_EVENT_ID, -1);
            }else if(mRegistrationMode == MODE_MANUAL_REGISTRATION){

                Workshop workshop = extras.getParcelable(KEY_WORKSHOP);
                WalkInUser walkInUser = extras.getParcelable(KEY_WALK_IN_USER);

                if(workshop != null && walkInUser != null){
                    mWorkshop = workshop;
                    mWalkInUser = walkInUser;
                }
            }else{
                // Something went wrong.
                finish();
            }
        }

        mDrawingView = (DrawingView) findViewById(R.id.digsig_drawing_area);
        mClearButton = (Button) findViewById(R.id.digsig_button_clear);
        mSubmitButton = (Button) findViewById(R.id.digsig_button_submit);
        mProgressView = (FrameLayout) findViewById(R.id.digsig_progress);

        mClearButton.setOnClickListener(this);
        mSubmitButton.setOnClickListener(this);

        mProgressView.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View view) {
        if(mDrawingView != null){
            //FrameLayout container = (FrameLayout) findViewById(R.id.test_digsig_container);
           if(view == mClearButton){
               mDrawingView.clearDrawing();
               //container.setVisibility(View.GONE);
           }else if(view == mSubmitButton){
               // submit after checking if the signature has been drawn
               Log.d("~~", "Has user drawn? " + mDrawingView.hasUserDrawn());

               if(mDrawingView.hasUserDrawn()){

                   mSignatureBitmap = mDrawingView.getDrawnBitmap();

                   /*container.setVisibility(View.VISIBLE);

                   ImageView img = (ImageView) findViewById(R.id.test_digsig);

                   img.setImageBitmap(mSignatureBitmap);*/

                   AppCompatDialog dialog = new AlertDialog.Builder(this)
                           .setMessage(R.string.prompt_send_signature)
                           .setPositiveButton(R.string.label_send, new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialogInterface, int i) {
                                   submitDigitalSignature(mSignatureBitmap);
                               }
                           })
                           .setNegativeButton(R.string.label_cancel, new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialogInterface, int i) {
                                   // Do nothing
                               }
                           })
                           .create();
                   dialog.show();

               }
           }
        }
    }

    private void submitDigitalSignature(Bitmap signatureBitmap){


        mProgressView.setVisibility(View.VISIBLE);

        String email = SecurePreferencesHelper.retrieveEmail(this);
        String authToken = SecurePreferencesHelper.retrieveAuthToken(this);

        // Change based on the mode
        String url = "";
        JSONObject jsonParams = null;
        int httpMethod = -123123;
        if(mRegistrationMode == MODE_QR_SCAN) {

            // Prepare the parameters. We pass in an empty value for "category" since we want to get all Summit workshops
            String idParams =  mUserEventID >= 0? (mUserEventID + "?") : "?";

            String params = String.format("%1$s"
                            + API.SubmitDigitalSignature.PARAM_USER_EMAIL + "=%2$s&"
                            + API.SubmitDigitalSignature.PARAM_USER_TOKEN + "=%3$s",
                    idParams,
                    email,
                    authToken);

            Log.d("~~", "--- params: " + params);

            url = API.url() + API.SubmitDigitalSignature.method() + params;

            try {
                jsonParams = generateJSONParams(signatureBitmap);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // Set the http method
            httpMethod = API.SubmitDigitalSignature.httpMethod();

        }else if(mRegistrationMode == MODE_MANUAL_REGISTRATION){

            // Prepare the parameters.
            String params = String.format(API.ManualRegistration.PARAM_USER_EMAIL + "=%1$s&"
                            + API.ManualRegistration.PARAM_USER_TOKEN + "=%2$s",
                    email,
                    authToken);

            Log.d("~~", "--- params: " + params);

            url = API.url() + API.ManualRegistration.method() + params;

            try {
                jsonParams = generateJSONParams(signatureBitmap);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // Set the http method
            httpMethod = API.ManualRegistration.httpMethod();
        }

        Log.d("~~", "--- jsonParams: " + jsonParams);

        if(mRequest != null){
            mRequest.cancel();
        }

        /*mRequest = new JsonObjectRequest(httpMethod,
                url, jsonParams, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        Log.d("~~", "--- response: " + response);

                        setResult(RESULT_OK);
                        finish();

                        mProgressView.setVisibility(View.GONE);
                    }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error);
                mProgressView.setVisibility(View.GONE);

                String body = "";

                //get response body and parse with appropriate encoding
                if(error.networkResponse.data!=null) {
                    try {
                        body = new String(error.networkResponse.data,"UTF-8");

                        JSONObject errorMessage = new JSONObject(body);

                        Log.d(TAG, "error body: " + errorMessage);

                        if(errorMessage.has("errors") && !errorMessage.getString("errors").isEmpty()){
                            showDialogAlert(getResources().getString(R.string.label_error), errorMessage.getString("errors"));
                        }else if(errorMessage.has("base") && errorMessage.getJSONArray("base").length() > 0){
                            showDialogAlert(getResources().getString(R.string.label_error), errorMessage.getJSONArray("base").get(0).toString());
                        }else{
                            // Generic error
                            showDialogAlert(getResources().getString(R.string.label_error), getResources().getString(R.string.error_generic_try_again));
                        }
                    } catch (UnsupportedEncodingException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }) {

            *//**
             * Passing some request headers
             * *//*
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };*/

        mRequest = new JsonObjectRequest(httpMethod,
                url, jsonParams, this, this){

            /**
             * Passing some request headers
            * */
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };

        VolleyInstance.getInstance(this).addToRequestQueue(mRequest);
    }

    private JSONObject generateJSONParams(Bitmap signatureBitmap) throws JSONException {
        JSONObject params = new JSONObject();

        // Encode the bitmap to Base64
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        signatureBitmap.compress(Bitmap.CompressFormat.PNG, 50, byteArrayOutputStream);
        byte[] yourByteArray = byteArrayOutputStream.toByteArray();

        String signatureEncodedString = Base64.encodeToString(yourByteArray, Base64.DEFAULT);


        if(mRegistrationMode == MODE_QR_SCAN) {
            // Create the user_event object
            JSONObject userEventParams = new JSONObject();
            userEventParams.put(API.SubmitDigitalSignature.JSON_STATUS, API.SubmitDigitalSignature.JSON_STATUS_VALUE);
            // Create the user_event_digital_signature object
            JSONObject signatureParams = new JSONObject();
            signatureParams.put(API.SubmitDigitalSignature.JSON_SIGNATURE, signatureEncodedString);

            // Add these to the main parameter object
            params.put(API.SubmitDigitalSignature.JSON_USER_EVENT, userEventParams);
            params.put(API.SubmitDigitalSignature.JSON_DIGITAL_SIGNATURE, signatureParams);
        }else if(mRegistrationMode == MODE_MANUAL_REGISTRATION){
            // Create the walk in user
            JSONObject walkInUserParams = new JSONObject();
            walkInUserParams.put(API.ManualRegistration.JSON_USER_EMAIL, mWalkInUser.getEmail());
            walkInUserParams.put(API.ManualRegistration.JSON_EVENT_BATCH_CREDIT, mWorkshop.getCreditValue());
            walkInUserParams.put(API.ManualRegistration.JSON_USER_NAME, mWalkInUser.getName());
            walkInUserParams.put(API.ManualRegistration.JSON_EMPLOYEE_ID, mWalkInUser.getEmployeeID());
            walkInUserParams.put(API.ManualRegistration.JSON_CONTACT_NUMBER, mWalkInUser.getContactNumber());
            walkInUserParams.put(API.ManualRegistration.JSON_STATUS, mWalkInUser.getStatus());


            // Create the user_event_digital_signature object
            JSONObject signatureParams = new JSONObject();
            signatureParams.put(API.ManualRegistration.JSON_SIGNATURE, signatureEncodedString);

            params.put(API.ManualRegistration.JSON_EVENT_WALKIN_USER, walkInUserParams);
            params.put(API.ManualRegistration.JSON_EVENT_BATCH_ID, mWorkshop.getId());
            params.put(API.ManualRegistration.JSON_DIGITAL_SIGNATURE, signatureParams);
        }

        return params;
    }


    private void showDialogAlert(String title, String message){
        Dialog mDialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.label_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mProgressView.setVisibility(View.GONE);
                    }
                }).create();
        mDialog.show();
    }

    @Override
    public void onResponse(JSONObject response) {
        Log.d(TAG, response.toString());
        Log.d("~~", "--- response: " + response);

        setResult(RESULT_OK);
        finish();

        mProgressView.setVisibility(View.GONE);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

        VolleyLog.d(TAG, "Error: " + error);
        mProgressView.setVisibility(View.GONE);

        String body = "";

        //get response body and parse with appropriate encoding
        if(error.networkResponse.data!=null) {
            try {
                body = new String(error.networkResponse.data,"UTF-8");

                JSONObject errorMessage = new JSONObject(body);

                Log.d(TAG, "error body: " + errorMessage);

                if(errorMessage.has("errors") && !errorMessage.getString("errors").isEmpty()){
                    showDialogAlert(getResources().getString(R.string.label_error), errorMessage.getString("errors"));
                }else if(errorMessage.has("base") && errorMessage.getJSONArray("base").length() > 0){
                    showDialogAlert(getResources().getString(R.string.label_error), errorMessage.getJSONArray("base").get(0).toString());
                }else{
                    // Generic error
                    showDialogAlert(getResources().getString(R.string.label_error), getResources().getString(R.string.error_generic_try_again));
                }
            } catch (UnsupportedEncodingException | JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
