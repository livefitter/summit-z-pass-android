package com.uobsummit.zpass.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import com.uobsummit.zpass.adapters.viewholders.WorkshopsBindingHolder;
import com.uobsummit.zpass.eventhandlers.WorkshopClickHandler;
import com.uobsummit.zpass.models.Workshop;

import java.util.ArrayList;

/**
 * Created by LloydM on 7/7/16
 * for Livefitter
 */
public class WorkshopsAdapter extends RecyclerView.Adapter<WorkshopsBindingHolder>{

    private RecyclerView mRecyclerView;
    private ArrayList<Workshop> mWorkshopList;
    private LayoutInflater mLayoutInflater;

    // tracking for selected views
    public int mSelectedPosition = -1;
    public View mSelectedView;

    public WorkshopsAdapter(ArrayList<Workshop> workshopArrayList, Context context, RecyclerView recyclerView){
        this.mWorkshopList = workshopArrayList;
        this.mRecyclerView = recyclerView;
        mLayoutInflater = LayoutInflater.from(context);

        // initialize selection tracking
        mSelectedPosition = 0;
    }

    public void setWorkshopList(ArrayList<Workshop> workshopArrayList){
        this.mWorkshopList = workshopArrayList;
        notifyDataSetChanged();

        // reset selection tracking
        mSelectedPosition = 0;
        mSelectedView = null;
    }

    @Override
    public WorkshopsBindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        WorkshopsBindingHolder holder = WorkshopsBindingHolder.create(mLayoutInflater, parent);
        View rootView = holder.getBinding().getRoot();

        rootView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("~~", "inner click. is selected? " + view.isSelected());
                if (!view.isSelected()) {
                    // We are selecting the view clicked
                    if (mSelectedView != null) {
                        // deselect the previously selected view
                        mSelectedView.setSelected(false);
                    }
                    mSelectedPosition = mRecyclerView.getChildAdapterPosition(view);
                    mSelectedView = view;

                    // toggle the item clicked
                    view.setSelected(!view.isSelected());
                }/* else {
                    // We are deselecting the view clicked
                    mSelectedPosition = -1;
                    mSelectedView = null;
                }*/
            }
        });

        return holder;

        //return WorkshopsBindingHolder.create(mLayoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(WorkshopsBindingHolder holder, int position) {
        holder.bindTo(mWorkshopList.get(position));
        if(position == mSelectedPosition){
            holder.getBinding().getRoot().setSelected(true);
            // Keep track of the currently selected view when recycled
            mSelectedView = holder.getBinding().getRoot();
        }else{
            holder.getBinding().getRoot().setSelected(false);
        }
    }

    @Override
    public int getItemCount() {
        return mWorkshopList != null? mWorkshopList.size() : 0;
    }

    public Workshop getSelectedWorkshop(){
        if(mWorkshopList != null && !mWorkshopList.isEmpty()){
            if(mSelectedPosition >=0 ){
                return mWorkshopList.get(mSelectedPosition);
            }
        }

        return null;
    }
}
