package com.uobsummit.zpass.adapters.viewholders;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.uobsummit.zpass.databinding.EventsItemBinding;
import com.uobsummit.zpass.eventhandlers.EventBatchClickHandler;
import com.uobsummit.zpass.models.EventBatch;

/**
 * Created by LloydM on 7/18/16
 * for Livefitter
 */

public class EventsBindingHolder extends RecyclerView.ViewHolder {
    private EventsItemBinding mBinding;

    public static EventsBindingHolder create(LayoutInflater inflater, ViewGroup parent) {
        EventsItemBinding binding = EventsItemBinding
                .inflate(inflater, parent, false);
        return new EventsBindingHolder(binding);
    }

    private EventsBindingHolder(EventsItemBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public ViewDataBinding getBinding() {
        return mBinding;
    }

    public void bindTo(EventBatch eventBatch, EventBatchClickHandler clickHandler) {
        mBinding.setEventbatch(eventBatch);
        mBinding.setHandler(clickHandler);
        mBinding.executePendingBindings();
    }
}
