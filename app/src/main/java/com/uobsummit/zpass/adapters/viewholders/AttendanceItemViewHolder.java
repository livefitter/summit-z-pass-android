package com.uobsummit.zpass.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.uobsummit.zpass.R;


/**
 * Created by LloydM on 11/28/16
 * for Livefitter
 */

public class AttendanceItemViewHolder extends RecyclerView.ViewHolder {

    TextView idView, nameView, statusView;

    public AttendanceItemViewHolder(View root) {
        super(root);

        idView = (TextView) root.findViewById(R.id.attendance_item_id);
        nameView = (TextView) root.findViewById(R.id.attendance_item_name);
        statusView = (TextView) root.findViewById(R.id.attendance_item_status);
    }

    public TextView getIdView() {
        return idView;
    }

    public TextView getNameView() {
        return nameView;
    }

    public TextView getStatusView() {
        return statusView;
    }
}
