package com.uobsummit.zpass.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.uobsummit.zpass.R;

import java.util.ArrayList;

/**
 * Created by LloydM on 6/24/16
 * for Livefitter
 */
public class ScanSessionAdapter extends RecyclerView.Adapter<ScanSessionAdapter.ScanSessionViewHolder> {

    Context mContext;
    private ArrayList<String> mScannedData;
    private int mLastPosition = -1;


    public ScanSessionAdapter(Context con) {
        this.mContext = con;
        this.mScannedData = new ArrayList<String>();
    }

    public void updateDataList(String newData) {
        this.mScannedData.add(newData);
        notifyItemInserted(mScannedData.size() - 1);
    }

    public static class ScanSessionViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public LinearLayout mRootLayout;
        public TextView mCounter;
        public TextView mResultData;
        public ScanSessionViewHolder(View v) {
            super(v);
            mRootLayout = (LinearLayout) v;
            mCounter = (TextView) mRootLayout.findViewById(R.id.scanner_recyclerview_row_counter);
            mResultData = (TextView) mRootLayout.findViewById(R.id.scanner_recyclerview_row_data);
        }

        public void clearAnimation()
        {
            mRootLayout.clearAnimation();
        }
    }

    @Override
    public ScanSessionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.scanning_session_list_item, parent, false);

        ScanSessionViewHolder vh = new ScanSessionViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ScanSessionViewHolder holder, int position) {
        String counterText = Integer.toString(position + 1);
        holder.mCounter.setText(counterText);
        holder.mResultData.setText(mScannedData.get(position));

        // Here you apply the animation when the view is bound
        setAnimation(holder.mRootLayout, position);
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > mLastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            mLastPosition = position;
        }
    }

    @Override
    public void onViewDetachedFromWindow(ScanSessionViewHolder holder) {
        ((ScanSessionViewHolder)holder).clearAnimation();
    }

    @Override
    public int getItemCount() {
        return mScannedData.size();
    }
}
