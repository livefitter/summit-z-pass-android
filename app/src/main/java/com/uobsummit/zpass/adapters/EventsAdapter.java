package com.uobsummit.zpass.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.uobsummit.zpass.adapters.viewholders.EventsBindingHolder;
import com.uobsummit.zpass.eventhandlers.EventBatchClickHandler;
import com.uobsummit.zpass.models.EventBatch;

import java.util.ArrayList;

/**
 * Created by LloydM on 7/7/16
 * for Livefitter
 */
public class EventsAdapter extends RecyclerView.Adapter<EventsBindingHolder>{

    private Context mContext;
    private ArrayList<EventBatch> mEventBatchList;
    private LayoutInflater mLayoutInflater;
    private EventBatchClickHandler mClickHandler;

    public EventsAdapter(ArrayList<EventBatch> eventBatches, Context context, EventBatchClickHandler clickHandler){
        this.mEventBatchList = eventBatches;
        this.mContext = context;
        mLayoutInflater = LayoutInflater.from(context);
        this.mClickHandler = clickHandler;
    }

    @Override
    public EventsBindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
//        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
//        EventsItemBinding binding = EventsItemBinding.inflate(inflater, parent, false);
        //return new EventsBindingHolder(binding);

        return EventsBindingHolder.create(mLayoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(EventsBindingHolder holder, int position) {
        holder.bindTo(mEventBatchList.get(position), mClickHandler);
    }

    @Override
    public int getItemCount() {
        return mEventBatchList != null? mEventBatchList.size() : 0;
    }


}
