package com.uobsummit.zpass.adapters;

/**
 * Created by LloydM on 6/21/16
 * for Livefitter
 */

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.uobsummit.zpass.R;

import java.util.ArrayList;

/**
 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class EventDetailsPagerAdapter extends FragmentPagerAdapter {

    public static int TOTAL_PAGE_COUNT = 2;

    private ArrayList<Fragment> mFragmentList;
    private Context mContext;
    
    public EventDetailsPagerAdapter(FragmentManager fm, Context context, ArrayList<Fragment> fragmentList) {
        super(fm);

        mFragmentList = fragmentList;

        this.mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (mFragmentList != null && mFragmentList.isEmpty() != true){
            return mFragmentList.get(position);
        }
        return null;
    }

    @Override
    public int getCount() {
        return TOTAL_PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.title_main_course);
            case 1:
                return mContext.getString(R.string.title_activity_view_attendance);
        }
        return null;
    }
}
