package com.uobsummit.zpass.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uobsummit.zpass.R;
import com.uobsummit.zpass.adapters.viewholders.AttendanceHeaderViewHolder;
import com.uobsummit.zpass.adapters.viewholders.AttendanceItemViewHolder;

import java.util.ArrayList;


/**
 * Created by LloydM on 11/28/16
 * for Livefitter
 */

public class AttendanceAdapter extends RecyclerView.Adapter<ViewHolder> {

    // Mode of operation
    public static final int MODE_PRE_REGISTERED = 1222;
    public static final int MODE_WALK_IN = 1225;

    // Keys for the dataList array
    public static final String KEY_COUNT_CHECKED_IN = "Attendance check in count";
    public static final String KEY_COUNT_PARTICIPANTS = "Attendance participant count";
    public static final String KEY_PARTICIPANT_NAME = "Attendance name";
    public static final String KEY_PARTICIPANT_STATUS = "Attendance status";

    // View types
    private final int TYPE_HEADER = 0;
    private final int TYPE_ITEM = 1;

    private Context context;
    private ArrayList<Bundle> dataList;
    private int mode;

    public AttendanceAdapter(Context context, int mode){
        this.context = context;
        this.mode = mode;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_HEADER){
            View view = LayoutInflater.from(context).inflate(R.layout.attendance_list_header, parent, false);
            return new AttendanceHeaderViewHolder(view);
        }else{
            View view = LayoutInflater.from(context).inflate(R.layout.attendance_list_item, parent, false);
            return new AttendanceItemViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        Bundle data = dataList.get(position);

        if(viewType == TYPE_HEADER){
            AttendanceHeaderViewHolder viewHolder = (AttendanceHeaderViewHolder) holder;

            String counterText = "";
            // Check the mode first
            if(mode == MODE_PRE_REGISTERED){
                counterText = String.format(context.getString(R.string.attendance_label_preregistered), data.getInt(KEY_COUNT_CHECKED_IN), data.getInt(KEY_COUNT_PARTICIPANTS));
            }else if(mode == MODE_WALK_IN){
                counterText = String.format(context.getString(R.string.attendance_label_walkins), data.getInt(KEY_COUNT_PARTICIPANTS));
            }

            viewHolder.getCounterView().setText(counterText);
        }else if(viewType == TYPE_ITEM){
            AttendanceItemViewHolder viewHolder = (AttendanceItemViewHolder) holder;

            // Since this is just the order of participants, the dataList index starts at 0 for the header
            // and all 1 to N indices refer to the participants, just set idText to the position in the list
            int idText = position;
            String nameText = data.getString(KEY_PARTICIPANT_NAME);
            String statusText = data.getString(KEY_PARTICIPANT_STATUS);

            viewHolder.getIdView().setText(String.valueOf(idText));
            viewHolder.getNameView().setText(nameText);
            viewHolder.getStatusView().setText(statusText);
        }
    }

    public void setDataList(ArrayList<Bundle> dataList){
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return dataList != null? dataList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0? TYPE_HEADER : TYPE_ITEM;
    }
}
