package com.uobsummit.zpass.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.uobsummit.zpass.R;


/**
 * Created by LloydM on 11/28/16
 * for Livefitter
 */

public class AttendanceHeaderViewHolder extends RecyclerView.ViewHolder {

    TextView counterView;

    public AttendanceHeaderViewHolder(View root) {
        super(root);

        counterView = (TextView) root.findViewById(R.id.attendance_header_count);
    }

    public TextView getCounterView() {
        return counterView;
    }
}
