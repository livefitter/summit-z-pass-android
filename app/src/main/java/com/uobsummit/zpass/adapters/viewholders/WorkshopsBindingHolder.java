package com.uobsummit.zpass.adapters.viewholders;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.uobsummit.zpass.databinding.WorkshopsItemBinding;
import com.uobsummit.zpass.models.Workshop;

/**
 * Created by LloydM on 7/18/16
 * for Livefitter
 */

public class WorkshopsBindingHolder extends RecyclerView.ViewHolder {
    private WorkshopsItemBinding mBinding;

    public static WorkshopsBindingHolder create(LayoutInflater inflater, ViewGroup parent) {
        WorkshopsItemBinding binding = WorkshopsItemBinding
                .inflate(inflater, parent, false);
        return new WorkshopsBindingHolder(binding);
    }

    private WorkshopsBindingHolder(WorkshopsItemBinding binding) {
        super(binding.getRoot());
        binding.getRoot().setClickable(true);
        mBinding = binding;
    }

    public ViewDataBinding getBinding() {
        return mBinding;
    }

    public void bindTo(Workshop workshop) {
        mBinding.setWorkshop(workshop);
        mBinding.setParticipantsCount(workshop.getParticipants());
        mBinding.executePendingBindings();
    }
}
