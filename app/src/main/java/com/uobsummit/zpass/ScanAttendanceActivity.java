package com.uobsummit.zpass;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.zxing.ResultPoint;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.BarcodeView;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.uobsummit.zpass.constants.API;
import com.uobsummit.zpass.models.Workshop;
import com.uobsummit.zpass.preferences.SecurePreferencesHelper;
import com.uobsummit.zpass.requests.VolleyInstance;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.Manifest.permission.CAMERA;


public class ScanAttendanceActivity extends AppCompatActivity
    implements View.OnClickListener{

    private static final String TAG = "ScanAttendance";

    static final int REQUEST_DIGITAL_SIGNATURE = 1337;
    static final int REQUEST_MANUAL_REGISTRATION = 1338;

    /**
     * Id to identity CAMERA permission mRequest.
     */
    private static final int REQUEST_CAMERA = 1029;

    private static int SCANNING_DELAY_IN_MS = 2000;
    // Flag that disables the appearance of the alertDialog showing the rationale
    // for the camera permission after it has appeared once.
    private static boolean HAS_PROMPTED_CAMERA_RATIONALE = false;
    // Flag that prevents repeated requests for Camera permissions if it has been rejected
    private static boolean HAS_PERMISSION_BEEN_ALLOWED = true;

    CoordinatorLayout mLayout;
    DecoratedBarcodeView mBarcodeScannerView;
    Button mViewButton, mRegisterButton;
    TextView mCurrentCountView, mTotalCountView;
    AppCompatDialog mDialog;

    // Resource file to make it easier to retrieve String resources
    Resources res;

    // Workshop being currently used
    public static final String KEY_WORKSHOP = "QRScan_current_workshop";
    Workshop mWorkshop;
    // Counters for the current (checked in) and total (confirmed only) participant
    private int currentCount = 0;
    private int totalCount = 0;

    // API request
    ValidateQRCodeTask mValidateTask;
    CheckInTask mCheckInTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_attendance);

        mLayout = (CoordinatorLayout) findViewById(R.id.scan_attendance_layout);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();

        // Instantiate the workshop variable
        Workshop workshop = null;
        if(savedInstanceState != null){
            workshop = savedInstanceState.getParcelable(KEY_WORKSHOP);
        }else if(extras != null){
            workshop = extras.getParcelable(KEY_WORKSHOP);
        }

        if(workshop != null){
            mWorkshop = workshop;

            currentCount = mWorkshop.getParticipantsCheckedIn();
            totalCount = mWorkshop.getParticipants();
        }

        // Initialize this flag
        HAS_PERMISSION_BEEN_ALLOWED = true;

        res = getResources();

        // Ready the DecoratedBarcodeView
        mBarcodeScannerView = (DecoratedBarcodeView) findViewById(R.id.barcode_scanner);

        // Other UI elements
        mCurrentCountView = (TextView) findViewById(R.id.scan_count_current);
        mTotalCountView = (TextView) findViewById(R.id.scan_count_total);
        mViewButton = (Button) findViewById(R.id.scan_button_view);
        mRegisterButton = (Button) findViewById(R.id.scan_button_registration);
        // Click listeners
        mViewButton.setOnClickListener(this);
        mRegisterButton.setOnClickListener(this);
        // Initialize values for count views
        mCurrentCountView.setText(String.valueOf(currentCount));
        mTotalCountView.setText(String.valueOf(totalCount));
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mayUseCamera()){
            initializeScan();
            mBarcodeScannerView.resume();
        }else{
            disableScanning();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mBarcodeScannerView.pause();
    }

    private void initializeScan() {
        if(mBarcodeScannerView != null) {
            mBarcodeScannerView.decodeContinuous(mBarcodeCallback);
            mBarcodeScannerView.setStatusText(res.getString(R.string.label_scan_ready));
        }
    }

    private void disableScanning(){
        if(mBarcodeScannerView != null) {
            mBarcodeScannerView.pause();
            mBarcodeScannerView.getStatusView().setText(R.string.label_scan_disabled);
        }
    }

    private BarcodeCallback mBarcodeCallback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            boolean isScanSuccessful = false;

            int userID = -1;

            if(result != null){

                if (result.getText() != null) {
                    //isScanSuccessful = true;
                    // Update the adapter of our recyclerView
                    //mAdapter.updateDataList(result.getText());

                    Log.d("~~", "Scanned code is: " + result.getText());

                    try {
                        userID = Integer.decode(result.getText());

                        Log.d("~~", "userID: " + userID);

                        if(userID >= 0){
                            isScanSuccessful = true;
                        }else{
                            // Not a valid QR code
                            showDialogAlert(res.getString(R.string.label_error), res.getString(R.string.label_qrcode_invalid), false);
                        }
                    }catch(NumberFormatException e){
                        // Result is not a number
                        e.getStackTrace();
                        // Not a valid QR code
                        showDialogAlert(res.getString(R.string.label_error), res.getString(R.string.label_qrcode_invalid), false);
                    }
                }
            }

            // Set the status message on screen
            String statusMessage = isScanSuccessful? res.getString(R.string.label_scan_successful)
                    : res.getString(R.string.label_scan_failed);

            mBarcodeScannerView.setStatusText(statusMessage);

            // Delay the scanning if successful and scroll the list up
            if(isScanSuccessful){
                //mRecyclerView.scrollToPosition(mAdapter.getItemCount() - 1);
                // Stop the scanning for now
                mBarcodeScannerView.getBarcodeView().stopDecoding();
                validateQRCode(userID);
            }else{
                // Delay the scanning
                delayScanningFor(SCANNING_DELAY_IN_MS, "", res.getString(R.string.label_scan_ready));
            }

        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {

        }
    };


    /**
     * API call to validate the scanned QR code
     * @param userID
     */
    private void validateQRCode(int userID){
        // Change the status message of the scanner view
        mBarcodeScannerView.setStatusText(res.getString(R.string.label_qrcode_validating));

        int eventID = mWorkshop.getId();
        final String email = SecurePreferencesHelper.retrieveEmail(this);
        final String authToken = SecurePreferencesHelper.retrieveAuthToken(this);

        if(mValidateTask != null){
            mValidateTask.cancel(true);
        }

        mValidateTask = new ValidateQRCodeTask(this, userID, eventID, email, authToken);
        mValidateTask.execute();

        /*// Prepare the parameters. We pass in an empty value for "category" since we want to get all Summit workshops
        String params = String.format(API.ValidateQRCode.PARAM_USER_ID + "=%1$s&"
                        + API.ValidateQRCode.PARAM_EVENT_ID + "=%2$s&"
                        + API.ValidateQRCode.PARAM_USER_EMAIL + "=%3$s&"
                        + API.ValidateQRCode.PARAM_USER_TOKEN + "=%4$s",
                userID,
                eventID,
                email,
                authToken);

        Log.d("~~", "--- params: " + params);

        String url = API.url() + API.ValidateQRCode.method() + params;

        Log.d("~~", "--- url: " + url);

        if(mRequest != null){
            mRequest.cancel();
        }

        mRequest = new StringRequest(API.ValidateQRCode.httpMethod(), url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Received response. Parsing JSON");

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject != null ){


                        Log.d("~~", "--- response: " + jsonObject);

                        if(jsonObject.has("user_event_id") && jsonObject.has("valid")){
                            int userEventID = jsonObject.getInt("user_event_id");
                            if(jsonObject.getBoolean("valid") && userEventID > 0){
                                // Valid user ID!
                                if(mWorkshop.isCoreWorkshop()){
                                    // Go to digital signature
                                    goToDigitalSignature(userEventID);
                                }else {

                                    // Call the QR code process request to check in the user
                                    checkInUser(userEventID, email, authToken);
                                }
                            }
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError: " + error);

                String body = "";

                //get response body and parse with appropriate encoding
                if(error.networkResponse.data!=null) {
                    try {
                        body = new String(error.networkResponse.data,"UTF-8");

                        JSONObject errorMessage = new JSONObject(body);

                        if(errorMessage.has("errors") && !errorMessage.getString("errors").isEmpty()){
                            // Show the error message and delay the enabling of scanning
                            //delayScanningFor(SCANNING_DELAY_IN_MS, errorMessage.getString("errors"), res.getString(R.string.label_scan_ready));

                            showDialogAlert(res.getString(R.string.label_error), errorMessage.getString("errors"), true);
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        VolleyInstance.getInstance(this).addToRequestQueue(mRequest);*/
    }

    /**
     * API call to check in user when they are not required to sign their signature
     * @param userEventID
     * @param email Used for authentication
     * @param authToken Used for authentication
     */
    private void checkInUser(int userEventID, String email, String authToken){

        if(mCheckInTask != null){
            mCheckInTask.cancel(true);
        }

        mCheckInTask = new CheckInTask(this, userEventID, email, authToken);
        mCheckInTask.execute();

        /*// Prepare the parameters. We pass in an empty value for "category" since we want to get all Summit workshops
        String idParams =  userEventID >= 0? (userEventID + "?") : "?";

        String params = String.format("%1$s"
                        + API.SubmitDigitalSignature.PARAM_USER_EMAIL + "=%2$s&"
                        + API.SubmitDigitalSignature.PARAM_USER_TOKEN + "=%3$s",
                idParams,
                email,
                authToken);

        Log.d("~~", "--- params: " + params);

        String url = API.url() + API.SubmitDigitalSignature.method() + params;

        JSONObject jsonParams = null;
        try {
            jsonParams = generateJSONParams();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("~~", "--- jsonParams: " + jsonParams);

        if(mCheckInRequest != null){
            mCheckInRequest.cancel();
        }

        mCheckInRequest = new JsonObjectRequest(API.SubmitDigitalSignature.httpMethod(),
                url, jsonParams, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                Log.d("~~", "--- response: " + response);

                userCheckInSuccess(true);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                userCheckInFailure(res.getString(R.string.error_attendance_not_recorded));
            }
        }) {

            *//**
             * Passing some request headers
             * *//*
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }

        };

        VolleyInstance.getInstance(this).addToRequestQueue(mCheckInRequest);*/
    }

    private JSONObject generateJSONParams() throws JSONException {
        JSONObject params = new JSONObject();

        // Create the user_event object
        JSONObject userEventParams = new JSONObject();
        userEventParams.put(API.SubmitDigitalSignature.JSON_STATUS, API.SubmitDigitalSignature.JSON_STATUS_VALUE);

        // Add these to the main parameter object
        params.put(API.SubmitDigitalSignature.JSON_USER_EVENT, userEventParams);

        return params;
    }

    /**
     * Stops the decoding of QR codes but not the camera preview (see {@link BarcodeView#stopDecoding()})
     * and enables it again after a delay
     * @param delayInMilli determines the delay (in milliseconds) before enabling QR code decoding
     */
    private void delayScanningFor(int delayInMilli, String startStatusMessage, final String endStatusMessage){

        if(delayInMilli > 0 && mBarcodeScannerView != null) {

            mBarcodeScannerView.getBarcodeView().stopDecoding();

            if(!startStatusMessage.isEmpty()){
                mBarcodeScannerView.setStatusText(startStatusMessage);
            }

            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    mBarcodeScannerView.setStatusText(endStatusMessage);
                    mBarcodeScannerView.decodeContinuous(mBarcodeCallback);
                }
            }, delayInMilli);
        }
    }

    /**
     * Starts the Digital Signature activity for a result
     * @param user_event_id
     */
    private void goToDigitalSignature(int user_event_id) {
        Log.d(TAG, "Core Workshop detected. Requiring Digital signature");

        Intent intent = new Intent(this, DigitalSignatureActivity.class);
        intent.putExtra(DigitalSignatureActivity.KEY_REGISTRATION_MODE, DigitalSignatureActivity.MODE_QR_SCAN);
        intent.putExtra(DigitalSignatureActivity.KEY_USER_EVENT_ID, user_event_id);
        startActivityForResult(intent, REQUEST_DIGITAL_SIGNATURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == REQUEST_DIGITAL_SIGNATURE){
            if(resultCode == RESULT_OK){
                userCheckInSuccess(true);
            }else{
                userCheckInFailure(res.getString(R.string.error_qrcode_signature_not_captured));
            }
        }else if(requestCode == REQUEST_MANUAL_REGISTRATION){
            if(resultCode == RESULT_OK){
                userCheckInSuccess(false);
            }else{
                delayScanningFor(50, "", res.getString(R.string.label_scan_ready));
            }
        }
    }

    /**
     * Called when we have successfully checked in a user.
     * Iterates the currentCount, update the textView displaying it and show a dialog to the user
     */
    private void userCheckInSuccess(boolean increaseCounter){
        if(increaseCounter) {
            currentCount++;
            mWorkshop.setParticipantsCheckedIn(currentCount);
            mCurrentCountView.setText(String.valueOf(currentCount));
        }
        showDialogAlert(res.getString(R.string.label_registration_successful), res.getString(R.string.label_user_checked_in), true);
    }

    /**
     * Called when the user check in fails for some reason.
     * Prompt the user to scan the QR code again.
     */
    private void userCheckInFailure(String messageString){
        // The digital signature wasnt captured. Prompt the user to scan again
        showDialogAlert(res.getString(R.string.label_error), messageString, true);
    }

    @Override
    public void onClick(View view) {
        if(mWorkshop != null){
            if(view == mViewButton){
                // View attendance
                Intent intent = new Intent(this, ViewAttendanceActivity.class);
                intent.putExtra(ViewAttendanceActivity.KEY_EVENT_BATCH_ID, mWorkshop.getId());
                startActivity(intent);
            }else if(view == mRegisterButton){
                // Manual Registration
                Intent registerIntent = new Intent(this, ManualRegistrationActivity.class);
                registerIntent.putExtra(ManualRegistrationActivity.KEY_WORKSHOP, mWorkshop);
                startActivityForResult(registerIntent, REQUEST_MANUAL_REGISTRATION);
            }

        }
    }

    private void showDialogAlert(String title, String message, final boolean hasDelayBeforeScanEnables){
        mDialog = new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.label_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        // Just a tiny insignificant delay
                        if(hasDelayBeforeScanEnables) {
                            delayScanningFor(50, "", res.getString(R.string.label_scan_ready));
                        }
                    }
                }).create();
        mDialog.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // Store the mWorkshop value
        outState.putParcelable(KEY_WORKSHOP, mWorkshop);

        super.onSaveInstanceState(outState);
    }

    /**
     * Method to mRequest permission from the user to use the device's camera.
     * @return if permission to use the Camera has been allowed
     */
    private boolean mayUseCamera() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        else {
            // For Marshmallow+
            if (checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED) {
                return true;
            }
            if (shouldShowRequestPermissionRationale(CAMERA)) {
                Snackbar.make(mLayout, R.string.permission_rationale_camera, Snackbar.LENGTH_INDEFINITE)
                        .setAction(android.R.string.ok, new View.OnClickListener() {
                            @Override
                            @TargetApi(Build.VERSION_CODES.M)
                            public void onClick(View v) {
                                requestPermissions(new String[]{CAMERA}, REQUEST_CAMERA);
                            }
                        })
                        .show();
            } else {
                requestPermissions(new String[]{CAMERA}, REQUEST_CAMERA);
            }
        }
        return false;
    }

    /**
     * Callback received when a permissions mRequest has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA) {
            if (grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                initializeScan();
                mBarcodeScannerView.resume();
            }else{
                HAS_PERMISSION_BEEN_ALLOWED = false;
                disableScanning();

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, CAMERA)) {
                    // Show the permissions prompt as a expanded reason as to why
                    // this app needs camera permissions
                    // but only show it once to the user
                    if(!HAS_PROMPTED_CAMERA_RATIONALE) {
                        String title_res = res.getString(R.string.permission_prompt_camera_title);
                        String message_res = res.getString(R.string.permission_prompt_camera_message);
                        AppCompatDialog alert = createPermissionsPrompt(title_res, message_res);
                        alert.show();
                    }
                } else {
                    // ZalentsUser has been informed on the rationale but still denied permission
                    // Display a snackbar as a last ditch prompt to the user
                    Snackbar snack = Snackbar.make(mLayout, res.getString(R.string.permission_settings_prompt_camera), Snackbar.LENGTH_INDEFINITE)
                            .setAction(res.getString(R.string.label_settings), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                                    intent.setData(uri);
                                    startActivity(intent);
                                }
                            });

                    snack.show();
                }
            }
        }
    }

    /**
     * Creates a dialog that prompts the user to reconsider granting the app permission to use the camera
     * @param title String resource for the title
     * @param message String resource for the message
     * @return The alert dialog to show to the user
     */
    private AppCompatDialog createPermissionsPrompt(String title, String message){
        return new AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton(res.getString(R.string.label_retry), new DialogInterface.OnClickListener() {
                    @Override
                    @TargetApi(Build.VERSION_CODES.M)
                    public void onClick(DialogInterface dialog, int which) {
                        requestPermissions(new String[]{CAMERA}, REQUEST_CAMERA);
                    }
                })
                .setPositiveButton(res.getString(R.string.label_sure), null)
                .create();
    }

    private class ValidateQRCodeTask extends AsyncTask<Void,Void,Void>
        implements Response.Listener<String>,
        Response.ErrorListener{

        Context context;
        int userID, eventID;
        String email, authToken;

        StringRequest mRequest;

        public ValidateQRCodeTask(Context context, int userID, int eventID, String email, String authToken) {
            this.context = context;
            this.userID = userID;
            this.eventID = eventID;
            this.email = email;
            this.authToken = authToken;
        }

        @Override
        protected Void doInBackground(Void... voids) {

            // Prepare the parameters. We pass in an empty value for "category" since we want to get all Summit workshops
            String params = String.format(API.ValidateQRCode.PARAM_USER_ID + "=%1$s&"
                            + API.ValidateQRCode.PARAM_EVENT_ID + "=%2$s&"
                            + API.ValidateQRCode.PARAM_USER_EMAIL + "=%3$s&"
                            + API.ValidateQRCode.PARAM_USER_TOKEN + "=%4$s",
                    userID,
                    eventID,
                    email,
                    authToken);

            Log.d("~~", "--- params: " + params);

            String url = API.url() + API.ValidateQRCode.method() + params;

            Log.d("~~", "--- url: " + url);

            if(mRequest != null){
                mRequest.cancel();
            }

            mRequest = new StringRequest(API.ValidateQRCode.httpMethod(), url, this, this);

            VolleyInstance.getInstance(context).addToRequestQueue(mRequest);

            return null;
        }

        @Override
        public void onResponse(String response) {

            Log.d(TAG, "Received response. Parsing JSON");

            try {
                JSONObject jsonObject = new JSONObject(response);

                if(jsonObject != null ){


                    Log.d("~~", "--- response: " + jsonObject);

                    if(jsonObject.has("user_event_id") && jsonObject.has("valid")){
                        int userEventID = jsonObject.getInt("user_event_id");
                        if(jsonObject.getBoolean("valid") && userEventID > 0){
                            // Valid user ID!
                            if(mWorkshop.isCoreWorkshop()){
                                // Go to digital signature
                                goToDigitalSignature(userEventID);
                            }else {

                                // Call the QR code process request to check in the user
                                checkInUser(userEventID, email, authToken);
                            }
                        }
                    }

                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {

            Log.e(TAG, "VolleyError: " + error);

            String body = "";

            //get response body and parse with appropriate encoding
            if(error.networkResponse.data!=null) {
                try {
                    body = new String(error.networkResponse.data,"UTF-8");

                    JSONObject errorMessage = new JSONObject(body);

                    if(errorMessage.has("errors") && !errorMessage.getString("errors").isEmpty()){
                        // Show the error message and delay the enabling of scanning
                        //delayScanningFor(SCANNING_DELAY_IN_MS, errorMessage.getString("errors"), res.getString(R.string.label_scan_ready));

                        showDialogAlert(res.getString(R.string.label_error), errorMessage.getString("errors"), true);
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class CheckInTask extends AsyncTask<Void,Void,Void>
            implements Response.Listener<JSONObject>,
            Response.ErrorListener{

        Context context;
        int userEventID;
        String email, authToken;

        JsonObjectRequest mCheckInRequest;

        public CheckInTask(Context context, int userEventID, String email, String authToken) {
            this.context = context;
            this.userEventID = userEventID;
            this.email = email;
            this.authToken = authToken;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            // Prepare the parameters. We pass in an empty value for "category" since we want to get all Summit workshops
            String idParams =  userEventID >= 0? (userEventID + "?") : "?";

            String params = String.format("%1$s"
                            + API.SubmitDigitalSignature.PARAM_USER_EMAIL + "=%2$s&"
                            + API.SubmitDigitalSignature.PARAM_USER_TOKEN + "=%3$s",
                    idParams,
                    email,
                    authToken);

            Log.d("~~", "--- params: " + params);

            String url = API.url() + API.SubmitDigitalSignature.method() + params;

            JSONObject jsonParams = null;
            try {
                jsonParams = generateJSONParams();
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.d("~~", "--- jsonParams: " + jsonParams);

            if(mCheckInRequest != null){
                mCheckInRequest.cancel();
            }

            mCheckInRequest = new JsonObjectRequest(API.SubmitDigitalSignature.httpMethod(), url, jsonParams, this, this) {

                /**
                 * Passing some request headers
                 * */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    return headers;
                }

            };

            VolleyInstance.getInstance(context).addToRequestQueue(mCheckInRequest);

            return null;
        }

        @Override
        public void onResponse(JSONObject response) {

            Log.d(TAG, response.toString());
            Log.d("~~", "--- response: " + response);

            userCheckInSuccess(true);
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            VolleyLog.d(TAG, "Error: " + error.getMessage());
            userCheckInFailure(res.getString(R.string.error_attendance_not_recorded));
        }
    }
}
