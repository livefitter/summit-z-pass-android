package com.uobsummit.zpass;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.uobsummit.zpass.adapters.EventDetailsPagerAdapter;
import com.uobsummit.zpass.fragments.OutdatedAttendanceFragment;
import com.uobsummit.zpass.fragments.CourseFragment;
import com.uobsummit.zpass.models.EventBatch;

import java.util.ArrayList;

public class EventActivity extends AppCompatActivity {

    public static String INTENT_ARGS_EVENTBATCH = "EventActivity.args.eventbatch";

    private static String STATE_EVENTBATCH;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private EventDetailsPagerAdapter mEventDetailsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private EventBatch mEventBatch;

    private ArrayList<Fragment> mFragmentList;
    private CourseFragment mCourseFragment;
    private OutdatedAttendanceFragment mAttendanceFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        Log.d("~~", "EventActivity onCreate");

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if(savedInstanceState != null){

            Log.d("~~", "EventActivity onCreate 1");
            this.mEventBatch = savedInstanceState.getParcelable(STATE_EVENTBATCH);
        }else {


            Log.d("~~", "EventActivity onCreate 2");
            // Retrieve the Intent extras and pass them along to the fragments
            Bundle extras = getIntent().getExtras();

            if (extras != null && extras.containsKey(INTENT_ARGS_EVENTBATCH)) {
                this.mEventBatch = extras.getParcelable(INTENT_ARGS_EVENTBATCH);


                Log.d("~~", "EventActivity onCreate 3");
                if (mEventBatch != null && !mEventBatch.getTitle().isEmpty()) {

                    Log.d("~~", "EventActivity onCreate 4");
                    getSupportActionBar().setTitle(mEventBatch.getTitle());
                }
            }
        }

        // Initialize the fragments and add them to mFragmentList
        mFragmentList = new ArrayList<Fragment>();

        mCourseFragment = new CourseFragment();
        mAttendanceFragment = new OutdatedAttendanceFragment();

        mFragmentList.add(mCourseFragment);
        mFragmentList.add(mAttendanceFragment);

        Log.d("~~", "EventActivity onCreate 5");

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        if(mEventBatch != null) {

            Log.d("~~", "EventActivity onCreate 6");
            setEventBatchToFragments(mEventBatch);
            mEventDetailsPagerAdapter = new EventDetailsPagerAdapter(getSupportFragmentManager(), getApplicationContext(), mFragmentList);

            // Set up the ViewPager with the sections adapter.
            mViewPager = (ViewPager) findViewById(R.id.viewpager);
            mViewPager.setAdapter(mEventDetailsPagerAdapter);

            TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
            tabLayout.setupWithViewPager(mViewPager);
        }



        Log.d("~~", "EventActivity onCreate end");
    }

    public void setEventBatchToFragments(EventBatch eventBatch) {
        if(eventBatch!= null && mCourseFragment != null && mAttendanceFragment != null){
            mCourseFragment.setEventBatch(eventBatch);
            // mAttendanceFragment.setEventBatch(eventBatch)
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        Log.d("~~", "EventActivity onRestoreInstanceState");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("~~", "EventActivity onResume");
        Log.d("~~", "mEventBatch null: " + (mEventBatch == null));
        Log.d("~~", "mViewPager null: " + (mViewPager == null));
        Log.d("~~", "mEventDetailsPagerAdapter null: " + (mEventDetailsPagerAdapter == null));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("~~", "onActivityResult");
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            //TODO add the scanned data from the scanning session to the Attendance screen
            if(result.getContents() == null) {
                Log.d("EventActivity", "Cancelled scan");
                //Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            } else {

                Log.d("EventActivity", "Scanned");
                //Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
            }
        } else {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {

        Log.d("~~", "EventActivity onSaveInstanceState");
        // Save this activity's state
        outState.putParcelable(STATE_EVENTBATCH, mEventBatch);

        super.onSaveInstanceState(outState, outPersistentState);
    }
}
