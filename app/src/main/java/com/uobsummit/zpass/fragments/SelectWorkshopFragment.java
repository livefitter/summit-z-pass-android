package com.uobsummit.zpass.fragments;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.uobsummit.zpass.LoginActivity;
import com.uobsummit.zpass.ScanAttendanceActivity;
import com.uobsummit.zpass.R;
import com.uobsummit.zpass.ViewAttendanceActivity;
import com.uobsummit.zpass.adapters.WorkshopsAdapter;
import com.uobsummit.zpass.constants.API;
import com.uobsummit.zpass.eventhandlers.WorkshopClickHandler;
import com.uobsummit.zpass.models.Workshop;
import com.uobsummit.zpass.preferences.SecurePreferencesHelper;
import com.uobsummit.zpass.requests.VolleyInstance;
import com.uobsummit.zpass.requests.WorkshopListParser;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * A placeholder fragment containing a simple view.
 */
public class SelectWorkshopFragment extends Fragment
        implements View.OnClickListener{

    private static final String TAG = "SelectWorkshopFragment";

    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    WorkshopsAdapter mAdapter;
    FrameLayout mProgressView;
    LinearLayout mButtonLayout;
    Button mScanButton, mViewButton;

    // Workshop list request task
    RetrieveWorkshopListTask mTask;

    public SelectWorkshopFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_select_workshop, container, false);

        mRecyclerView = (RecyclerView) root.findViewById(R.id.workshop_recyclerview);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                mLayoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        // defer the setAdapter at a latter time

        // Retrieve references to the screen elements
        mProgressView = (FrameLayout) root.findViewById(R.id.workshop_progress);
        mButtonLayout = (LinearLayout) root.findViewById(R.id.workshop_button_layout);
        mScanButton = (Button) root.findViewById(R.id.workshop_button_scan);
        mViewButton = (Button) root.findViewById(R.id.workshop_button_view);
        // Button click listeners
        mScanButton.setOnClickListener(this);
        mViewButton.setOnClickListener(this);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        retrieveWorkshopList();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_select_workshop, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemID = item.getItemId();

        switch (itemID){
            case R.id.action_logout:
                logOutUser();
                break;
            case R.id.action_refresh:
                retrieveWorkshopList();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void retrieveWorkshopList() {
        //toggleProgressView(true);

        String email = SecurePreferencesHelper.retrieveEmail(getActivity());
        String authToken = SecurePreferencesHelper.retrieveAuthToken(getActivity());

        if(email.isEmpty() || authToken.isEmpty()){
            Log.e(TAG, "Incorrect login credentials. Logging user out");
            logOutUser();
        }

        if(mTask != null){
            // Cancel potential ongoing request
            mTask.cancel(true);
        }

        mTask = new RetrieveWorkshopListTask(email, authToken);
        mTask.execute();

        /*// Prepare the parameters. We pass in an empty value for "category" since we want to get all Summit workshops
        String params = String.format("" + API.GetListOfWorkshops.PARAM_CATEGORY + "=%1$s&"
                + API.GetListOfWorkshops.PARAM_USER_EMAIL + "=%2$s&"
                + API.GetListOfWorkshops.PARAM_USER_TOKEN + "=%3$s",
                "",
                email,
                authToken);

        Log.d("~~", "--- params: " + params);

        String url = API.url() + API.GetListOfWorkshops.method() + params;

        Log.d("~~", "--- retrieveWorkshopList url: " + url);

        if(mRequest != null){
            mRequest.cancel();
        }

        mRequest = new StringRequest(API.GetListOfWorkshops.httpMethod(), url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Received response. Parsing JSON");

                try {
                    JSONArray jsonArray = new JSONArray(response);

                    if(jsonArray != null ){

                        ArrayList<Workshop> workshopList = WorkshopListParser.parseJSON(jsonArray);
                        setDataList(workshopList);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError: " + error);

                *//*if(error instanceof AuthFailureError){
                    mEmailView.setError(getString(R.string.error_incorrect_login));
                    mEmailView.requestFocus();
                }*//*
            }
        });

        VolleyInstance.getInstance(getActivity()).addToRequestQueue(mRequest);*/
    }

    private void setDataList(ArrayList<Workshop> workshopList){
        toggleProgressView(false);
        if(mAdapter == null){
            mAdapter = new WorkshopsAdapter(workshopList, getActivity(), mRecyclerView);
            mRecyclerView.setAdapter(mAdapter);
        }else{
            mAdapter.setWorkshopList(workshopList);
        }
    }

    private void logOutUser(){
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    /**
     * Toggles on and off the progress views and the button layout
     * @param isVisible
     */
    private void toggleProgressView(boolean isVisible){
        if(mProgressView != null && mButtonLayout != null) {
            int visibility = isVisible ? View.VISIBLE : View.GONE;
            mProgressView.setVisibility(visibility);

            // Reverse it for the button layout
            visibility = isVisible ? View.GONE : View.VISIBLE;
            mButtonLayout.setVisibility(visibility);
        }
    }

    @Override
    public void onClick(View view) {
        if(mAdapter != null){
            // Retrieve the currently selected workshop
            Workshop selectedWorkshop = mAdapter.getSelectedWorkshop();

            if(selectedWorkshop != null){
                if(view == mScanButton){
                    // Scan attendance
                    Intent intent = new Intent(getActivity(), ScanAttendanceActivity.class);
                    intent.putExtra(ScanAttendanceActivity.KEY_WORKSHOP, selectedWorkshop);
                    startActivity(intent);
                }else if( view == mViewButton){
                    // View attendance
                    Intent intent = new Intent(getActivity(), ViewAttendanceActivity.class);
                    intent.putExtra(ViewAttendanceActivity.KEY_EVENT_BATCH_ID, selectedWorkshop.getId());
                    startActivity(intent);
                }

            }
        }
    }

    private class RetrieveWorkshopListTask extends AsyncTask<Void,Void,Boolean> implements Response.ErrorListener, Response.Listener<String> {

        String email, authToken;
        // Workshop list request
        StringRequest mRequest;

        public RetrieveWorkshopListTask(String email, String authToken) {
            this.email = email;
            this.authToken = authToken;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            toggleProgressView(true);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            // Prepare the parameters. We pass in an empty value for "category" since we want to get all Summit workshops
            String params = String.format("" + API.GetListOfWorkshops.PARAM_CATEGORY + "=%1$s&"
                            + API.GetListOfWorkshops.PARAM_USER_EMAIL + "=%2$s&"
                            + API.GetListOfWorkshops.PARAM_USER_TOKEN + "=%3$s",
                    "",
                    email,
                    authToken);

            Log.d("~~", "--- params: " + params);

            String url = API.url() + API.GetListOfWorkshops.method() + params;

            Log.d("~~", "--- retrieveWorkshopList url: " + url);

            if(mRequest != null){
                mRequest.cancel();
            }

            mRequest = new StringRequest(API.GetListOfWorkshops.httpMethod(), url, this, this);

            /*mRequest = new StringRequest(API.GetListOfWorkshops.httpMethod(), url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d(TAG, "Received response. Parsing JSON");

                    try {
                        JSONArray jsonArray = new JSONArray(response);

                        if(jsonArray != null ){

                            ArrayList<Workshop> workshopList = WorkshopListParser.parseJSON(jsonArray);
                            setDataList(workshopList);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "VolleyError: " + error);

                *//*if(error instanceof AuthFailureError){
                    mEmailView.setError(getString(R.string.error_incorrect_login));
                    mEmailView.requestFocus();
                }*//*
                }
            });*/

            VolleyInstance.getInstance(getActivity()).addToRequestQueue(mRequest);

            return true;
        }

        @Override
        protected void onCancelled(Boolean result) {
            super.onCancelled(result);
            toggleProgressView(false);
            mRequest.cancel();
        }

        @Override
        protected void onPostExecute(Boolean result) {
            super.onPostExecute(result);
            toggleProgressView(false);
        }

        @Override
        public void onResponse(String response) {
            Log.d(TAG, "Received response. Parsing JSON");

            try {
                JSONArray jsonArray = new JSONArray(response);

                if(jsonArray != null ){

                    ArrayList<Workshop> workshopList = WorkshopListParser.parseJSON(jsonArray);
                    setDataList(workshopList);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            Log.e(TAG, "VolleyError: " + error);
        }
    }
}
