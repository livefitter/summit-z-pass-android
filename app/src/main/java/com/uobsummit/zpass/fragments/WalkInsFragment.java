package com.uobsummit.zpass.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.uobsummit.zpass.R;
import com.uobsummit.zpass.adapters.AttendanceAdapter;
import com.uobsummit.zpass.constants.API;
import com.uobsummit.zpass.preferences.SecurePreferencesHelper;
import com.uobsummit.zpass.requests.VolleyInstance;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;


/**
 * Created by LloydM on 11/28/16
 * for Livefitter
 */

public class WalkInsFragment extends BaseAttendanceFragment {

    private static final String TAG = "WalkInsFragment";

    RetrieveParticipantsTask mTask;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        mAdapter = new AttendanceAdapter(getActivity(), AttendanceAdapter.MODE_WALK_IN);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_attendance, container, false);

        // Prepare recycler view
        mRecyclerView = (RecyclerView) root.findViewById(R.id.attendance_list);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        // Get a reference to the progress bar and hide it for now
        mProgress = (FrameLayout) root.findViewById(R.id.attendance_progress);
        showProgress(false);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();

        retrieveParticipantsList();
    }

    @Override
    public void retrieveParticipantsList() {
        // Only continue if there is a valid event batch ID
        if(mEventBatchID == -1){
            return;
        }

        showProgress(true);

        String email = SecurePreferencesHelper.retrieveEmail(getActivity());
        String authToken = SecurePreferencesHelper.retrieveAuthToken(getActivity());

        if(mTask != null){
            // Cancel potential ongoing request
            mTask.cancel(true);
        }

        mTask = new RetrieveParticipantsTask(email, authToken, mEventBatchID);
        mTask.execute();

        /*// Prepare the parameters. We pass in an empty value for "category" since we want to get all Summit workshops
        String params = String.format("" + API.AttendanceWalkIn.PARAM_EVENT + "=%1$d&"
                        + API.AttendanceWalkIn.PARAM_USER_EMAIL + "=%2$s&"
                        + API.AttendanceWalkIn.PARAM_USER_TOKEN + "=%3$s",
                mEventBatchID,
                email,
                authToken);

        Log.d("~~", "--- params: " + params);

        String url = API.url() + API.AttendanceWalkIn.method() + params;

        if(mRequest != null){
            mRequest.cancel();
        }

        mRequest = new StringRequest(API.AttendanceWalkIn.httpMethod(), url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "Received response. Parsing JSON");

                Log.d("~~", "--- response: " + response);

                boolean isResponseCorrect = false;

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    if(jsonObject != null){
                        if(jsonObject.has("participants_count") && jsonObject.has("participants")) {
                            isResponseCorrect = true;
                            handleJSONResult(jsonObject);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(!isResponseCorrect){

                    // Generic error
                    showDialogAlert(getResources().getString(R.string.label_error), getResources().getString(R.string.error_generic_try_again));
                }

            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "VolleyError: " + error);

                *//*if(error instanceof AuthFailureError){
                    mEmailView.setError(getString(R.string.error_incorrect_login));
                    mEmailView.requestFocus();
                }*//*

                String body = "";

                //get response body and parse with appropriate encoding
                if(error.networkResponse.data!=null) {
                    try {
                        body = new String(error.networkResponse.data,"UTF-8");

                        JSONObject errorMessage = new JSONObject(body);

                        if(errorMessage.has("errors") && !errorMessage.getString("errors").isEmpty()){
                            showDialogAlert(getResources().getString(R.string.label_error), errorMessage.getString("errors"));
                        }else if(errorMessage.has("base") && errorMessage.getJSONArray("base").length() > 0){
                            showDialogAlert(getResources().getString(R.string.label_error), errorMessage.getJSONArray("base").get(0).toString());
                        }else{
                            // Generic error
                            showDialogAlert(getResources().getString(R.string.label_error), getResources().getString(R.string.error_generic_try_again));
                        }
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        VolleyInstance.getInstance(getActivity()).addToRequestQueue(mRequest);*/
    }

    /**
     * Parses the JSONObject response and feeds the data to the adapter for displaying
     * @param jsonResponse
     */
    private void handleJSONResult(JSONObject jsonResponse) throws JSONException {
        // Prepare the Bundle list that will contain our data
        ArrayList<Bundle> dataList = new ArrayList<>();
        Bundle data = new Bundle();

        // Get the checked-in and participants count
        // and place them in our dataList at index 0
        int participantsCount = jsonResponse.getInt("participants_count");
        data.putInt(AttendanceAdapter.KEY_COUNT_PARTICIPANTS, participantsCount);
        dataList.add(data);

        // Traverse the participants JSON array
        JSONArray participantsJSONArray = jsonResponse.getJSONArray("participants");
        // and place them in our dataList
        for(int i = 0; i < participantsJSONArray.length(); i++){
            data = new Bundle();
            JSONObject participantJSONObj = participantsJSONArray.getJSONObject(i);

            if(participantJSONObj == null){
                continue;
            }

            //int participantID = participantJSONObj.getInt("id");
            String participantName = participantJSONObj.getString("name");
            String participantStatus = participantJSONObj.getString("status");

            data.putString(AttendanceAdapter.KEY_PARTICIPANT_NAME, participantName);
            data.putString(AttendanceAdapter.KEY_PARTICIPANT_STATUS, participantStatus);
            dataList.add(data);
        }

        if(mAdapter != null ){
            mAdapter.setDataList(dataList);

            showProgress(false);
        }
    }


    private void showDialogAlert(String title, String message){
        Dialog mDialog = new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(R.string.label_ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        showProgress(false);
                    }
                }).create();
        mDialog.show();
    }

    private class RetrieveParticipantsTask extends AsyncTask<Void,Void,Boolean>
            implements Response.Listener<String>, Response.ErrorListener{

        String email, authToken;
        int eventBatchID;

        StringRequest mRequest;

        public RetrieveParticipantsTask(String email, String authToken, int eventBatchID) {
            this.email = email;
            this.authToken = authToken;
            this.eventBatchID = eventBatchID;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            // Prepare the parameters. We pass in an empty value for "category" since we want to get all Summit workshops
            String params = String.format("" + API.AttendanceWalkIn.PARAM_EVENT + "=%1$d&"
                            + API.AttendanceWalkIn.PARAM_USER_EMAIL + "=%2$s&"
                            + API.AttendanceWalkIn.PARAM_USER_TOKEN + "=%3$s",
                    mEventBatchID,
                    email,
                    authToken);

            Log.d("~~", "--- params: " + params);

            String url = API.url() + API.AttendanceWalkIn.method() + params;

            if(mRequest != null){
                mRequest.cancel();
            }

            /*mRequest = new StringRequest(API.AttendanceWalkIn.httpMethod(), url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e(TAG, "Received response. Parsing JSON");

                    Log.d("~~", "--- response: " + response);

                    boolean isResponseCorrect = false;

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        if(jsonObject != null){
                            if(jsonObject.has("participants_count") && jsonObject.has("participants")) {
                                isResponseCorrect = true;
                                handleJSONResult(jsonObject);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    if(!isResponseCorrect){

                        // Generic error
                        showDialogAlert(getResources().getString(R.string.label_error), getResources().getString(R.string.error_generic_try_again));
                    }

                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e(TAG, "VolleyError: " + error);

                *//*if(error instanceof AuthFailureError){
                    mEmailView.setError(getString(R.string.error_incorrect_login));
                    mEmailView.requestFocus();
                }*//*

                    String body = "";

                    //get response body and parse with appropriate encoding
                    if(error.networkResponse.data!=null) {
                        try {
                            body = new String(error.networkResponse.data,"UTF-8");

                            JSONObject errorMessage = new JSONObject(body);

                            if(errorMessage.has("errors") && !errorMessage.getString("errors").isEmpty()){
                                showDialogAlert(getResources().getString(R.string.label_error), errorMessage.getString("errors"));
                            }else if(errorMessage.has("base") && errorMessage.getJSONArray("base").length() > 0){
                                showDialogAlert(getResources().getString(R.string.label_error), errorMessage.getJSONArray("base").get(0).toString());
                            }else{
                                // Generic error
                                showDialogAlert(getResources().getString(R.string.label_error), getResources().getString(R.string.error_generic_try_again));
                            }
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });*/

            mRequest = new StringRequest(API.AttendanceWalkIn.httpMethod(), url, this, this);

            VolleyInstance.getInstance(getActivity()).addToRequestQueue(mRequest);

            return true;
        }

        @Override
        public void onResponse(String response) {
            Log.e(TAG, "Received response. Parsing JSON");

            Log.d("~~", "--- response: " + response);

            boolean isResponseCorrect = false;

            try {
                JSONObject jsonObject = new JSONObject(response);

                if(jsonObject != null){
                    if(jsonObject.has("participants_count") && jsonObject.has("participants")) {
                        isResponseCorrect = true;
                        handleJSONResult(jsonObject);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(!isResponseCorrect){

                // Generic error
                showDialogAlert(getResources().getString(R.string.label_error), getResources().getString(R.string.error_generic_try_again));
            }
        }

        @Override
        public void onErrorResponse(VolleyError error) {
            Log.e(TAG, "VolleyError: " + error);
            String body = "";

            //get response body and parse with appropriate encoding
            if(error.networkResponse.data!=null) {
                try {
                    body = new String(error.networkResponse.data,"UTF-8");

                    JSONObject errorMessage = new JSONObject(body);

                    if(errorMessage.has("errors") && !errorMessage.getString("errors").isEmpty()){
                        showDialogAlert(getResources().getString(R.string.label_error), errorMessage.getString("errors"));
                    }else if(errorMessage.has("base") && errorMessage.getJSONArray("base").length() > 0){
                        showDialogAlert(getResources().getString(R.string.label_error), errorMessage.getJSONArray("base").get(0).toString());
                    }else{
                        // Generic error
                        showDialogAlert(getResources().getString(R.string.label_error), getResources().getString(R.string.error_generic_try_again));
                    }
                } catch (UnsupportedEncodingException | JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
