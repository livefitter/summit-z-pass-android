package com.uobsummit.zpass.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.uobsummit.zpass.R;
import com.uobsummit.zpass.adapters.AttendanceAdapter;


/**
 * Created by LloydM on 11/28/16
 * for Livefitter
 */

public abstract class BaseAttendanceFragment extends Fragment {

    public static final String KEY_EVENT_ID = "eventBatchID";

    FrameLayout mProgress;
    RecyclerView mRecyclerView;
    LinearLayoutManager mLayoutManager;
    AttendanceAdapter mAdapter;

    int mEventBatchID = -1;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();

        if(arguments != null){
            mEventBatchID = arguments.getInt(KEY_EVENT_ID, -1);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_attendance, container, false);

        // Prepare recycler view
        mRecyclerView = (RecyclerView) root.findViewById(R.id.attendance_list);
        mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        // Get a reference to the progress bar and hide it for now
        mProgress = (FrameLayout) root.findViewById(R.id.attendance_progress);
        showProgress(false);

        return root;
    }

    protected void showProgress(boolean show){
        if(mProgress != null){
            int visibility = show? View.VISIBLE : View.GONE;
            mProgress.setVisibility(visibility);
        }
    }
    /**
     * Call the API to retrieve the participant list
     */
    public abstract void retrieveParticipantsList();
}
