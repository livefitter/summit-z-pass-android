package com.uobsummit.zpass.fragments;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.google.zxing.integration.android.IntentIntegrator;
import com.uobsummit.zpass.ScanAttendanceActivity;
import com.uobsummit.zpass.R;
import com.uobsummit.zpass.databinding.FragmentOutdatedCourseBinding;
import com.uobsummit.zpass.models.EventBatch;

/**
 * Created by LloydM
 */
//TODO remove later
public class CourseFragment extends Fragment
    implements View.OnClickListener{

    private EventBatch mEventBatch;

    private IntentIntegrator mIntentIntegrator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Use data binding to inflate the view
        FragmentOutdatedCourseBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_outdated_course, container, false);
        View rootView = binding.getRoot();
        if(mEventBatch != null){
            binding.setEventbatch(mEventBatch);
            binding.setHandler(this);
        }

        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemID = item.getItemId();

        switch (itemID){

        }
        return super.onOptionsItemSelected(item);
    }

    public void startQRScan() {
        Intent intent = new Intent(getActivity(), ScanAttendanceActivity.class);
        //startActivity(intent);
        startActivityForResult(intent, 012);

        /*mIntentIntegrator = new IntentIntegrator(getActivity()).setOrientationLocked(false).setCaptureActivity(ScanAttendanceActivity.class);
        mIntentIntegrator.initiateScan();*/
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d("~~", "CourseFragment onResume");
    }
    
    public void setEventBatch(EventBatch eventBatch){
        this.mEventBatch = eventBatch;
    }

    @Override
    public void onClick(View v) {
        startQRScan();
    }
}