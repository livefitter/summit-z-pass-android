package com.uobsummit.zpass.eventhandlers;

import com.uobsummit.zpass.models.EventBatch;

/**
 * Created by LloydM on 7/18/16
 * for Livefitter
 */

public interface EventBatchClickHandler{
    public void onEventBatchClick(EventBatch eventBatch);
}
