package com.uobsummit.zpass.eventhandlers;

import android.view.View;

import com.uobsummit.zpass.models.Workshop;

/**
 * Created by LloydM on 7/18/16
 * for Livefitter
 */

public interface WorkshopClickHandler {
    public void onWorkshopClick(View clickedView, Workshop workshop);
}
