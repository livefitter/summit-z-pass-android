package com.uobsummit.zpass.utils;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by LloydM on 11/15/16
 * for Livefitter
 */

public class ZpassUtil {

    public static void hideSoftKeyboard(AppCompatActivity activity){
        View view = activity.getCurrentFocus();

        if(view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);

            //imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
            imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}
