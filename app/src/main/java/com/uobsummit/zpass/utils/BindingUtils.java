package com.uobsummit.zpass.utils;

import android.databinding.BindingAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by LloydM on 7/15/16
 * for Livefitter
 */

public class BindingUtils {

    /**
     * Loads a image URI into an imageview
     * @param view
     * @param uri
     */
    @BindingAdapter("app:imageUrl")
    public static void bitmapSource(ImageView view, String uri){
        //Picasso.with(view.getContext()).load(uri).into(view);
        Glide.with(view.getContext()).load(uri).into(view);
    }

    /**
     * Loads a image resource into an imageview
     * @param view
     * @param res
     */
    @BindingAdapter("app:imageRes")
    public static void bitmapSource(ImageView view, int res){
        //Picasso.with(view.getContext()).load(res).into(view);
        Glide.with(view.getContext()).load(res).into(view);
    }

    /**
     * Collects and concatinates an entire string list content
     * and puuts it into a textview
     * @param view
     * @param contentArray
     */
    //@BindingAdapter("app:content_array")
    public static void contentArrayToTextView(TextView view, ArrayList<String> contentArray){
        if(contentArray!= null && !contentArray.isEmpty()){
            String textContent = "";
            for(int i = 0; i < contentArray.size(); i++){
                String content = contentArray.get(i);

                textContent = textContent.concat(content);

                // Add a newline character for indexes not yet the last
                if(i < contentArray.size() - 1){
                    textContent = textContent.concat("\n");
                }
            }

            view.setText(textContent);
        }else{
            String textContent = "None";
            view.setText(textContent);
        }
    }
}
