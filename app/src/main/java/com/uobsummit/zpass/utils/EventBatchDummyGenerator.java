package com.uobsummit.zpass.utils;

import android.util.Log;

import com.uobsummit.zpass.R;
import com.uobsummit.zpass.models.EventBatch;
import com.uobsummit.zpass.models.UserEvent;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

/**
 * Created by LloydM on 7/14/16
 * for Livefitter
 */
public class EventBatchDummyGenerator {

    public static ArrayList<EventBatch> generate(){
        ArrayList<EventBatch> eventBatches = new ArrayList<EventBatch>();

        String dateToday = generateDateToday();
        String startTime = generateTime(0);
        String endTime = generateTime(1);

        EventBatch ev = new EventBatch();
        ev.setTitle("Terrarium Making sdfsdf sdf sd fsd fsd fsdf sdf sd fsdf sdf sdfsdf sdf sdf sd fsdf sdf sd fsd fsdf sdf sdf sfsdf sdf wer wer wer wer wer wer wer werw erw er w");
        ev.setEventTitle("July");
        ev.setTempImageRes(R.drawable.july);
        ev.setStartedAtDate(dateToday);
        ev.setStartTime(startTime);
        ev.setEndTime(endTime);
        eventBatches.add(ev);

        startTime = generateTime(1);
        endTime = generateTime(2);

        ev = new EventBatch();
        ev.setTitle("Leatherwork");
        ev.setEventTitle("July");
        ev.setTempImageRes(R.drawable.august);
        ev.setStartedAtDate(dateToday);
        ev.setStartTime(startTime);
        ev.setEndTime(endTime);
        eventBatches.add(ev);

        startTime = generateTime(2);
        endTime = generateTime(3);

        ev = new EventBatch();
        ev.setTitle("Wood carving");
        ev.setEventTitle("July");
        ev.setTempImageRes(R.drawable.november);
        ev.setStartedAtDate(dateToday);
        ev.setStartTime(startTime);
        ev.setEndTime(endTime);
        eventBatches.add(ev);

        startTime = generateTime(4);
        endTime = generateTime(5);

        ev = new EventBatch();
        ev.setTitle("Wood carving");
        ev.setEventTitle("July");
        ev.setTempImageRes(R.drawable.january);
        ev.setStartedAtDate(dateToday);
        ev.setStartTime(startTime);
        ev.setEndTime(endTime);
        eventBatches.add(ev);

        startTime = generateTime(5);
        endTime = generateTime(6);

        ev = new EventBatch();
        ev.setTitle("Wood carving B");
        ev.setEventTitle("July");
        ev.setTempImageRes(R.drawable.february);
        ev.setStartedAtDate(dateToday);
        ev.setStartTime(startTime);
        ev.setEndTime(endTime);
        eventBatches.add(ev);

        startTime = generateTime(6);
        endTime = generateTime(7);

        ev = new EventBatch();
        ev.setTitle("Glass blowing");
        ev.setEventTitle("July");
        ev.setTempImageRes(R.drawable.december);
        ev.setStartedAtDate(dateToday);
        ev.setStartTime(startTime);
        ev.setEndTime(endTime);
        eventBatches.add(ev);

        startTime = generateTime(7);
        endTime = generateTime(8);

        ev = new EventBatch();
        ev.setTitle("Leatherwork Intermediate");
        ev.setEventTitle("July");
        ev.setTempImageRes(R.drawable.august);
        ev.setStartedAtDate(dateToday);
        ev.setStartTime(startTime);
        ev.setEndTime(endTime);
        eventBatches.add(ev);

        return eventBatches;
    }

    // 2016-06-15T08:00:00+08:00
    //y-MM-dd'T'HH:mm:ssZ --> 2016-06-15T08:00:00+0800

    //TODO add processing for proper trailing words for ordinal numbers (i.e. 1st instead of 1th)
    private static String generateDateToday(){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd MMM y");
        return df.format(cal.getTime());
    }

    private static String generateTime(int offset){
        Calendar cal = Calendar.getInstance();

        if(offset != 0) {
            cal.add(Calendar.HOUR_OF_DAY, offset);
        }

        SimpleDateFormat df = new SimpleDateFormat("hh:mma");
        return df.format(cal.getTime());
    }
}
